-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 28, 2019 at 03:43 AM
-- Server version: 5.6.35
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `upbrigh1_seshbuddy`
--

-- --------------------------------------------------------

--
-- Table structure for table `friends`
--

CREATE TABLE `friends` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(11) NOT NULL,
  `friend_id` bigint(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `notification_settings`
--

CREATE TABLE `notification_settings` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userId` bigint(20) NOT NULL,
  `shmoke` int(11) NOT NULL DEFAULT '1',
  `match` int(11) NOT NULL DEFAULT '1',
  `drop` int(11) NOT NULL DEFAULT '1',
  `smo` int(11) NOT NULL DEFAULT '1',
  `deals` int(11) NOT NULL DEFAULT '1',
  `smo_iou` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `notification_settings`
--

INSERT INTO `notification_settings` (`id`, `uid`, `userId`, `shmoke`, `match`, `drop`, `smo`, `deals`, `smo_iou`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 1, 1, 1, 1, 1, 1, '2019-02-23 15:39:17', '2019-02-23 15:39:17');

-- --------------------------------------------------------

--
-- Table structure for table `push_notifications`
--

CREATE TABLE `push_notifications` (
  `id` int(11) NOT NULL,
  `userId` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `payload` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_read` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `push_notifications`
--

INSERT INTO `push_notifications` (`id`, `userId`, `title`, `payload`, `created_at`, `updated_at`, `is_read`) VALUES
(1, 1, 'You have invited to a sesh.', '{\"type\":\"shmoke\",\"strain\":\"skywalker OG\",\"time\":\"2:00 PM PST\",\"gram\":\".5 g\",\"date\":\"1\\/2\\/20018\",\"location\":\"MICHAEL ST\",\"utensils\":\"coffee mug\",\"buddies\":[1],\"flavour\":\"flower\",\"userId\":\"1\",\"members\":[1],\"status\":1,\"sesh_id\":17,\"action\":1}', '2019-02-28 16:50:42', '2019-02-28 16:50:42', 0);

-- --------------------------------------------------------

--
-- Table structure for table `seshes`
--

CREATE TABLE `seshes` (
  `id` int(10) UNSIGNED NOT NULL,
  `userId` bigint(10) UNSIGNED NOT NULL,
  `type` varchar(100) NOT NULL,
  `flavour` varchar(255) NOT NULL,
  `point` varchar(100) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `strain` varchar(100) NOT NULL,
  `time` varchar(30) NOT NULL,
  `gram` varchar(100) NOT NULL,
  `date` varchar(100) NOT NULL,
  `location` varchar(255) NOT NULL,
  `utensils` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `seshes`
--

INSERT INTO `seshes` (`id`, `userId`, `type`, `flavour`, `point`, `reason`, `strain`, `time`, `gram`, `date`, `location`, `utensils`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'shmoke', 'flower', '', '', 'skywalker OG', '2:00 PM PST', '.5 g', '1/2/20018', 'MICHAEL ST', 'coffee mug', 0, '2019-02-23 15:42:18', '2019-02-23 15:42:18'),
(2, 1, 'shmoke', 'flower', '', '', 'skywalker OG', '2:00 PM PST', '.5 g', '1/2/20018', 'MICHAEL ST', 'coffee mug', 1, '2019-02-28 15:52:50', '2019-02-28 15:52:50'),
(3, 1, 'shmoke', 'flower', '', '', 'skywalker OG', '2:00 PM PST', '.5 g', '1/2/20018', 'MICHAEL ST', 'coffee mug', 1, '2019-02-28 15:52:55', '2019-02-28 15:52:55'),
(4, 1, 'shmoke', 'flower', '', '', 'skywalker OG', '2:00 PM PST', '.5 g', '1/2/20018', 'MICHAEL ST', 'coffee mug', 1, '2019-02-28 15:53:02', '2019-02-28 15:53:02'),
(5, 1, 'shmoke', 'flower', '', '', 'skywalker OG', '2:00 PM PST', '.5 g', '1/2/20018', 'MICHAEL ST', 'coffee mug', 1, '2019-02-28 15:53:45', '2019-02-28 15:53:45'),
(6, 1, 'shmoke', 'flower', '', '', 'skywalker OG', '2:00 PM PST', '.5 g', '1/2/20018', 'MICHAEL ST', 'coffee mug', 1, '2019-02-28 16:07:05', '2019-02-28 16:07:05'),
(7, 1, 'shmoke', 'flower', '', '', 'skywalker OG', '2:00 PM PST', '.5 g', '1/2/20018', 'MICHAEL ST', 'coffee mug', 1, '2019-02-28 16:07:46', '2019-02-28 16:07:46'),
(8, 1, 'shmoke', 'flower', '', '', 'skywalker OG', '2:00 PM PST', '.5 g', '1/2/20018', 'MICHAEL ST', 'coffee mug', 1, '2019-02-28 16:08:21', '2019-02-28 16:08:21'),
(9, 1, 'shmoke', 'flower', '', '', 'skywalker OG', '2:00 PM PST', '.5 g', '1/2/20018', 'MICHAEL ST', 'coffee mug', 1, '2019-02-28 16:09:42', '2019-02-28 16:09:42'),
(10, 1, 'shmoke', 'flower', '', '', 'skywalker OG', '2:00 PM PST', '.5 g', '1/2/20018', 'MICHAEL ST', 'coffee mug', 1, '2019-02-28 16:10:07', '2019-02-28 16:10:07'),
(11, 1, 'shmoke', 'flower', '', '', 'skywalker OG', '2:00 PM PST', '.5 g', '1/2/20018', 'MICHAEL ST', 'coffee mug', 1, '2019-02-28 16:10:29', '2019-02-28 16:10:29'),
(12, 1, 'shmoke', 'flower', '', '', 'skywalker OG', '2:00 PM PST', '.5 g', '1/2/20018', 'MICHAEL ST', 'coffee mug', 1, '2019-02-28 16:11:35', '2019-02-28 16:11:35'),
(13, 1, 'shmoke', 'flower', '', '', 'skywalker OG', '2:00 PM PST', '.5 g', '1/2/20018', 'MICHAEL ST', 'coffee mug', 1, '2019-02-28 16:12:10', '2019-02-28 16:12:10'),
(14, 1, 'shmoke', 'flower', '', '', 'skywalker OG', '2:00 PM PST', '.5 g', '1/2/20018', 'MICHAEL ST', 'coffee mug', 1, '2019-02-28 16:12:53', '2019-02-28 16:12:53'),
(15, 1, 'shmoke', 'flower', '', '', 'skywalker OG', '2:00 PM PST', '.5 g', '1/2/20018', 'MICHAEL ST', 'coffee mug', 1, '2019-02-28 16:14:59', '2019-02-28 16:14:59'),
(16, 1, 'shmoke', 'flower', '', '', 'skywalker OG', '2:00 PM PST', '.5 g', '1/2/20018', 'MICHAEL ST', 'coffee mug', 1, '2019-02-28 16:23:17', '2019-02-28 16:23:17'),
(17, 1, 'shmoke', 'flower', '', '', 'skywalker OG', '2:00 PM PST', '.5 g', '1/2/20018', 'MICHAEL ST', 'coffee mug', 2, '2019-02-28 16:50:42', '2019-02-28 16:50:42');

-- --------------------------------------------------------

--
-- Table structure for table `sesh_and_boddies`
--

CREATE TABLE `sesh_and_boddies` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(11) NOT NULL,
  `sesh_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `reviewed` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sesh_and_boddies`
--

INSERT INTO `sesh_and_boddies` (`id`, `user_id`, `sesh_id`, `status`, `reviewed`, `created_at`) VALUES
(1, 1, 1, 0, 0, '2019-02-23 15:42:18'),
(2, 2, 1, 0, 0, '2019-02-23 15:42:18'),
(3, 1, 2, 1, 0, '2019-02-28 15:52:50'),
(4, 1, 2, 2, 0, '2019-02-28 15:52:50'),
(5, 1, 3, 1, 0, '2019-02-28 15:52:55'),
(6, 1, 3, 2, 0, '2019-02-28 15:52:55'),
(7, 1, 4, 1, 0, '2019-02-28 15:53:02'),
(8, 1, 4, 2, 0, '2019-02-28 15:53:02'),
(9, 1, 5, 1, 0, '2019-02-28 15:53:45'),
(10, 1, 5, 2, 0, '2019-02-28 15:53:45'),
(11, 1, 6, 1, 0, '2019-02-28 16:07:05'),
(12, 1, 6, 2, 0, '2019-02-28 16:07:05'),
(13, 1, 7, 1, 0, '2019-02-28 16:07:46'),
(14, 1, 7, 2, 0, '2019-02-28 16:07:46'),
(15, 1, 8, 1, 0, '2019-02-28 16:08:21'),
(16, 1, 8, 2, 0, '2019-02-28 16:08:21'),
(17, 1, 9, 1, 0, '2019-02-28 16:09:42'),
(18, 1, 9, 2, 0, '2019-02-28 16:09:42'),
(19, 1, 10, 1, 0, '2019-02-28 16:10:07'),
(20, 1, 10, 2, 0, '2019-02-28 16:10:07'),
(21, 1, 11, 1, 0, '2019-02-28 16:10:29'),
(22, 1, 11, 2, 0, '2019-02-28 16:10:29'),
(23, 1, 12, 1, 0, '2019-02-28 16:11:35'),
(24, 1, 12, 2, 0, '2019-02-28 16:11:35'),
(25, 1, 13, 1, 0, '2019-02-28 16:12:10'),
(26, 1, 13, 2, 0, '2019-02-28 16:12:10'),
(27, 1, 14, 1, 0, '2019-02-28 16:12:53'),
(28, 1, 14, 2, 0, '2019-02-28 16:12:53'),
(29, 1, 15, 1, 0, '2019-02-28 16:14:59'),
(30, 1, 15, 2, 0, '2019-02-28 16:14:59'),
(31, 1, 16, 1, 0, '2019-02-28 16:23:17'),
(32, 1, 16, 2, 0, '2019-02-28 16:23:17'),
(33, 1, 17, 1, 0, '2019-02-28 16:50:42'),
(34, 1, 17, 2, 0, '2019-02-28 16:50:42');

-- --------------------------------------------------------

--
-- Table structure for table `sesh_reviews`
--

CREATE TABLE `sesh_reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `reviewer_id` bigint(10) UNSIGNED NOT NULL,
  `userId` bigint(20) NOT NULL,
  `sesh_id` int(10) UNSIGNED NOT NULL,
  `quantity` float UNSIGNED NOT NULL,
  `quality` float UNSIGNED NOT NULL,
  `rolls` float UNSIGNED NOT NULL,
  `review` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sesh_reviews`
--

INSERT INTO `sesh_reviews` (`id`, `reviewer_id`, `userId`, `sesh_id`, `quantity`, `quality`, `rolls`, `review`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 5, 4.4, 5, 'The Review field is required.', '2019-02-23 15:43:09', '2019-02-23 15:43:09'),
(2, 1, 2, 1, 5, 4.4, 5, 'The Review field is required. user second user', '2019-02-23 15:43:09', '2019-02-23 15:43:09'),
(3, 1, 3, 1, 5, 4.4, 5, 'The Review field is required. user 3 review', '2019-02-23 15:43:09', '2019-02-23 15:43:09'),
(4, 1, 1, 1, 5, 4.4, 5, 'The Review field is required.', '2019-02-24 22:53:05', '2019-02-24 22:53:05'),
(5, 1, 2, 1, 5, 4.4, 5, 'The Review field is required. user second user', '2019-02-24 22:53:05', '2019-02-24 22:53:05'),
(6, 1, 3, 1, 5, 4.4, 5, 'The Review field is required. user 3 review', '2019-02-24 22:53:05', '2019-02-24 22:53:05'),
(7, 1, 1, 1, 5, 4.4, 5, 'The Review field is required.', '2019-02-28 16:24:36', '2019-02-28 16:24:36'),
(8, 1, 2, 1, 5, 4.4, 5, 'The Review field is required. user second user', '2019-02-28 16:24:36', '2019-02-28 16:24:36'),
(9, 1, 3, 1, 5, 4.4, 5, 'The Review field is required. user 3 review', '2019-02-28 16:24:36', '2019-02-28 16:24:36'),
(10, 1, 1, 1, 5, 4.4, 5, 'The Review field is required.', '2019-02-28 16:50:36', '2019-02-28 16:50:36'),
(11, 1, 2, 1, 5, 4.4, 5, 'The Review field is required. user second user', '2019-02-28 16:50:36', '2019-02-28 16:50:36'),
(12, 1, 3, 1, 5, 4.4, 5, 'The Review field is required. user 3 review', '2019-02-28 16:50:36', '2019-02-28 16:50:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userId` bigint(20) NOT NULL,
  `firstName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lastName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `userName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `profilePic` text COLLATE utf8_unicode_ci NOT NULL,
  `coverPic` text COLLATE utf8_unicode_ci NOT NULL,
  `fcmToken` text COLLATE utf8_unicode_ci NOT NULL,
  `platform` int(10) UNSIGNED DEFAULT NULL,
  `loginType` int(1) NOT NULL COMMENT '1=>FACEBOOK , 2=>SNAPCHAT,3=>WITH_APP',
  `userStatus` int(1) NOT NULL DEFAULT '0' COMMENT '0,1,2',
  `favoriteStrain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `aboutMe` text COLLATE utf8_unicode_ci NOT NULL,
  `dob` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `gender` int(1) NOT NULL DEFAULT '0' COMMENT '0 for not chosen yet , 1 for male , 2 for Female',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `quantity` float NOT NULL,
  `quality` float NOT NULL,
  `rolls` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `firstName`, `lastName`, `userName`, `email`, `password`, `profilePic`, `coverPic`, `fcmToken`, `platform`, `loginType`, `userStatus`, `favoriteStrain`, `aboutMe`, `dob`, `gender`, `created_at`, `updated_at`, `quantity`, `quality`, `rolls`) VALUES
(1, '', '', 'Vinesh', 'vineshgoyal788@yopmail.com', '', '', '', '', NULL, 1, 2, '', '', '', 0, '2019-02-23 15:39:17', '2019-02-28 16:23:15', 5, 4.4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `user_devices`
--

CREATE TABLE `user_devices` (
  `id` int(10) UNSIGNED NOT NULL,
  `userId` bigint(10) UNSIGNED NOT NULL,
  `platform` int(10) UNSIGNED NOT NULL,
  `deviceId` text NOT NULL,
  `fcmToken` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_devices`
--

INSERT INTO `user_devices` (`id`, `userId`, `platform`, `deviceId`, `fcmToken`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'vineshgoyal788_device_1', 'vineshgoyal788_token_new_1', '2019-02-23 15:39:17', '2019-02-23 15:39:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `friends`
--
ALTER TABLE `friends`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_settings`
--
ALTER TABLE `notification_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uid` (`uid`);

--
-- Indexes for table `push_notifications`
--
ALTER TABLE `push_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seshes`
--
ALTER TABLE `seshes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sesh_and_boddies`
--
ALTER TABLE `sesh_and_boddies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sesh_reviews`
--
ALTER TABLE `sesh_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `user_devices`
--
ALTER TABLE `user_devices`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `friends`
--
ALTER TABLE `friends`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notification_settings`
--
ALTER TABLE `notification_settings`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `push_notifications`
--
ALTER TABLE `push_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `seshes`
--
ALTER TABLE `seshes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `sesh_and_boddies`
--
ALTER TABLE `sesh_and_boddies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `sesh_reviews`
--
ALTER TABLE `sesh_reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_devices`
--
ALTER TABLE `user_devices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
