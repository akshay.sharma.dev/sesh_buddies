<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

header('Access-Control-Allow-Origin:*');
header('Content-Type: application/json');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class User extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('User_model');
        $this->load->library('encryption');
        $this->load->helper('string');
        $this->load->helper('common-utility');
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->library('Authorization_Token');
        
    }
    
    public function registration_post() {
        try {
            Helper::rawInput();

//            $this->form_validation->set_rules('loginType', 'Login Type', 'trim|required');

            $this->form_validation->set_rules('email', 'Email ID', 'trim|required');
            $this->form_validation->set_rules('userName', 'User Name', 'trim|is_unique[users.userName]|alpha_numeric');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[100]');

            //$this->form_validation->set_rules('firstName', 'First Name', 'trim|required');
            //$this->form_validation->set_rules('lastName', 'Last Name', 'trim|required');
            $this->form_validation->set_rules('profilePic', 'Profile Pic', 'trim');
            $this->form_validation->set_rules('coverPic', 'Cover Pic', 'trim');
            $this->form_validation->set_rules('fcmToken', 'FCM Token', 'trim|required');
            
            $this->form_validation->set_rules('deviceId', 'Device ID', 'trim|required');
            if ($this->form_validation->run() === false) {
                $this->response(Helper::error($this->form_validation->error_array()));
                die;
            }

            $platform = $this->authorization_token->getPlatformOrException();
            $data = $this->input->post();
            $data['password'] = $this->encryption->encrypt($this->input->post('password'));
            $data['loginType'] = LOGIN_PROVIDER::APP;
            $data['platform'] = $platform;
            $emails = $this->User_model->check_email_exists( $data['email'], true );
            if( count($emails) == 1 ){
                $userData = $emails[0];
                if( $userData['is_deleted'] == 1 ){
                    $data['is_deleted'] = 0;
                    $this->User_model->updateProfile($userData['userId'], $data );

                    $token_data = [
                        "user_id" => $userData['userId'],
                        "email" => $userData['email'],
                        "firstName" => $userData['firstName']
                    ];

                    $token = $this->authorization_token->generateToken($token_data);

                    $result = Helper::userCollection( $userData );
                    $result['accessToken'] = $token;
                    $result['is_deleted'] = 0;
                    $this->response(Helper::success("Account has been created", $result));
                }else {
                    throw new Exception("Email already exists.");
                }
                
            } else if(count($emails) == 0) {
                $result = $this->User_model->registration($data);

                if (!$result) {
                    throw new Exception("Something went wrong.");
                }

                $token_data = [
                    "user_id" => $result['userId'],
                    "email" => $result['email'],
                    "firstName" => $result['firstName']
                ];
                $token = $this->authorization_token->generateToken($token_data);
                $result['accessToken'] = $token;
                $result = Helper::userCollection( $result );
                $this->response(Helper::success("Account has been created", $result));
            }else {
                throw new Exception("Something went wrong.");
            }
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()));
        }
    }

    private function paramsForFacebook() {
        $this->form_validation->set_rules('email', 'Email ID', 'trim|required');
        $this->form_validation->set_rules('userName', 'User Name', 'trim');

        $this->form_validation->set_rules('firstName', 'First Name', 'trim');
        $this->form_validation->set_rules('lastName', 'Last Name', 'trim');
        $this->form_validation->set_rules('profilePic', 'Profile Pic', 'trim');
        $this->form_validation->set_rules('coverPic', 'Cover Pic', 'trim');
        $this->form_validation->set_rules('accessToken', 'Access Token', 'trim');
        $this->form_validation->set_rules('fcmToken', 'FCM Token', 'trim|required');
    }

    private function paramsForAPP() {
//        $this->form_validation->set_rules('email'   , 'Email ID', 'trim|required|valid_email|is_unique[users.email]');

        $this->form_validation->set_rules('email', 'Email ID', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('fcmToken', 'FCM Token', 'trim|required');
    }

    public function login_post() {
        try {
            Helper::rawInput();
//            $headers = Helper::getHeadersData();

            $this->form_validation->set_rules('loginType', 'Login Type', 'trim|required');
            $this->form_validation->set_rules('deviceId', 'Device ID', 'trim|required');
            $loginType = $this->input->post('loginType');
            $platform = $this->authorization_token->getPlatformOrException();
            $postData = $this->input->post();
            $postData['platform'] = $platform;
            $result;

            /*
             * Facebook Login
             */
            if ($loginType == LOGIN_PROVIDER::FACEBOOK) {
                
                $this->paramsForFacebook();
                if ($this->form_validation->run() === false) {
                    $this->response(Helper::error($this->form_validation->error_array()));
                    die;
                }
                
                $result = $this->User_model->login_with_facebook( $postData );
            }
            /*
             * Snapchat Login
             */ 
            else if ($loginType == LOGIN_PROVIDER::SNAPCHAT) {
                $this->response(Helper::error("Under construction."));
                die;
            }
            /*
             * Local App Login
             */ 
            else if ($loginType == LOGIN_PROVIDER::APP) { // login with WITH_APP
                $this->paramsForAPP();

                if ($this->form_validation->run() === false) {
                    $this->response(Helper::error($this->form_validation->error_array()));
                    die;
                }
                $result = $this->User_model->login_with_app( $postData );

                if (!$result) {
                    $this->response(Helper::error("Email or password is invalid."));
                    die;
                }
            } else {
                $this->response(Helper::error("Please provide valid login type."));
                die;
            }

            if (isset($result['password'])) {
                unset($result['password']);
            }
            $result['fcmToken'] = $this->input->post('fcmToken');
            $result = Helper::userCollection( $result );
            $this->response(Helper::success("", $result));
            die;
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()));
        }
    }
    
    public function change_password_post() {
        try {
            
            Helper::rawInput();
            $userData = $this->authorization_token->userData();
            
            $this->form_validation->set_rules('loginType', 'Login Type', 'trim|required');
            $this->form_validation->set_rules('password', 'Current Password', 'trim|required|min_length[6]|max_length[100]');
            $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[6]|max_length[100]');
            $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|min_length[6]|max_length[100]|matches[new_password]');
            
            $loginType = $this->input->post('loginType');
            
            if ($this->form_validation->run() === false) {
                $this->response(Helper::error($this->form_validation->error_array()), 200);
                die;
            }
            
            if ($loginType != LOGIN_PROVIDER::APP) {
                throw new Exception("Invalide Request.");
            }
            
            $password = $this->input->post('password');
            
            $result = $this->User_model->check_user_password( $userData->user_id, $loginType , $password );
//            var_dump($result);die;
            if (!$result) {
                $this->response(Helper::error("Password is invalid."));
                die;
            }
            $data = [
                "password" => $this->encryption->encrypt($this->input->post('new_password'))
            ];
            
            $isPasswordChanged = $this->User_model->updateProfile($userData->user_id, $data , false);
            if( $isPasswordChanged ) {
                $this->response(Helper::success("Password has changed."), 200);
                die;
            } else {
                throw new Exception("Passord could not  change right now, try after some time.");
            }

        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()));
        }
    }

    public function edit_profile_post() {
        try {
            Helper::rawInput();
            $userData = $this->authorization_token->userData();
//            $this->form_validation->set_rules('firstName', 'First Name', 'trim|max_length[30]|min_length[3]');
//            $this->form_validation->set_rules('lastName', 'Last Name', 'trim|min_length[3]|max_length[30]');
            $this->form_validation->set_rules('userName', 'User Name', 'trim|alpha_numeric');

            $this->form_validation->set_rules('profilePic', 'Profile Pic', 'trim');
            $this->form_validation->set_rules('coverPic', 'Cover Pic', 'trim');
            $this->form_validation->set_rules('fcmToken', 'FCM Token', 'trim');
            $this->form_validation->set_rules('userStatus', 'User Status', 'trim|in_list[0,1,2]');
            $this->form_validation->set_rules('gender', 'Gender', 'trim');

            $this->form_validation->set_rules('aboutMe', 'About me');
            $this->form_validation->set_rules('address', 'Address');

            $this->form_validation->set_rules('dob', 'date of birth', 'trim');
            $this->form_validation->set_rules('favoriteStrain', 'favorite strain', 'trim');

            if ($this->form_validation->run() === false) {
                $this->response(Helper::error($this->form_validation->error_array()), 200);
                die;
            }
            
            $data = array(
                'userName' => $this->input->post('userName') ,
                'profilePic' => $this->input->post('profilePic'),
                'coverPic' => $this->input->post('coverPic'),
                'fcmToken' => $this->input->post('fcmToken'),
                'userStatus' => $this->input->post('userStatus'),
                'gender' => $this->input->post('gender'),
                'aboutMe' => $this->input->post('aboutMe'),
                'dob' => $this->input->post('dob'),
                'favoriteStrain' => $this->input->post('favoriteStrain'),
                'address' => $this->input->post('address')
            );
            
            $data = array_filter($data);
            
            if( !isset($_POST['firstName'])){
                $data['firstName'] = "";
            }else {
                $data['firstName'] = $_POST['firstName'];
            }
            if( !isset($_POST['lastName']) ){
                $data['lastName'] = "";
            }else{
                $data['lastName'] = $_POST['lastName'];
            }
            
  
            $result = $this->User_model->updateProfile($userData->user_id, $data);
            $result = Helper::userCollection( $result );
            $this->response(Helper::success("Profile has updated.", $result), 200);
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()));
        }
    }

    public function verify_age_post() {
        try {
            Helper::rawInput();
            $userData = $this->authorization_token->userData();
            $this->form_validation->set_rules('age_verified', 'Age verified status', 'trim|in_list[0,1]');
            
            if ($this->form_validation->run() === false) {
                $this->response(Helper::error($this->form_validation->error_array()), 200);
                die;
            }

            $data = array(
                'age_verified' => $this->input->post('age_verified')
            );

            $result = $this->User_model->updateProfile($userData->user_id, $data);
            $result = Helper::userCollection( $result );
            $this->response(Helper::success("Profile has updated.", $result), 200);
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()));
        }
    }
    /*
     * @param imageType (required) (int) (0 for Profile image, 1 for Cover image)
     */
    public function updateImage_post() {
        try {
//            print_r($_POST);die;
            $this->form_validation->set_rules('imageType', 'Image type', 'trim|required|in_list[0,1]');
            $userData = $this->authorization_token->userData();

            if ($this->form_validation->run() === false) {
                $this->response(Helper::error($this->form_validation->error_array()), 200);
                die;
            }
            
            $imageType = $this->input->post('imageType');
            
            $filesToUpload = $_FILES;

            if (empty($filesToUpload)) {
                throw new Exception("Image is required.");
            }

            if ($imageType == 0) {
                $config['upload_path'] = './assets/uploads/profile_pic/';
                $successMessage = "Profile image is updated successfully";
            } elseif ($imageType == 1) {
                $config['upload_path'] = './assets/uploads/cover_pic/';
                $successMessage = "Cover image is updated successfully";
            } else {
                throw new Exception("Image Type is invalid");
            }

            $new_name = time() . '_' . $_FILES["img_pic"]['name'];
            $config['file_name'] = $new_name;
            $config['allowed_types'] = 'gif|jpg|jpeg|png|GIF|JPG|PNG|JPEG';
            $config['max_size'] = 100000;
            $config['max_width'] = 20000;
            $config['max_height'] = 20000;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('img_pic')) {
                $this->response(Helper::error( $this->upload->display_errors() ), 200);
                die;
            } else {
                $avatar_data = array('upload_data' => $this->upload->data());
                $img_name = $avatar_data['upload_data']['file_name'];
            }
            
            $data = array('img_name' => $img_name, "imageType" => $imageType, 'userId' => $userData->user_id );
            $result = $this->User_model->updateImage( $data );
            
            $this->response( Helper::success( $successMessage, $result), 200 );
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()), 200);
        }
    }
    
    public function updateUserStatus_post() {
        try {
            Helper::rawInput();
            $userData = $this->authorization_token->userData();
            $this->form_validation->set_rules('status', 'User Status', 'trim|in_list[0,1,2]');
            if ($this->form_validation->run() === false) {
                $this->response(Helper::error($this->form_validation->error_array()));
                die;
            }

            $data = array( 'userStatus' => $this->input->post('status') );

            $result = $this->User_model->updateProfile($userData->user_id, $data);
            
            $userData = $this->User_model->check_userId_exists($userData->user_id, true);
            $userData = Helper::userCollection( (array)$userData );
            if (isset($userData['password'])) {
                unset($userData['password']);
            }
            $this->response(Helper::success("Status has updated.", $userData), 200);
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()), 200);
        }
        
    }

    public function logout_post() {
        try{
            Helper::rawInput();
            $this->form_validation->set_rules('deviceId', 'Device ID', 'trim|required');
            
            if ($this->form_validation->run() === false) {
                $this->response(Helper::error($this->form_validation->error_array()));
                die;
            }

            $userData = $this->authorization_token->userData();
//            $result = $this->User_model->updateProfile($userData->user_id, array( 'userStatus' => 0 ) );
            
            //delete device ID
            $this->load->model('User_device_model');
            $this->User_device_model->deleteDevice( [
                'userId' => $userData->user_id, 'deviceId' => $this->input->post('deviceId') 
            ]);
            
            $this->response(Helper::success("You are logged out from our system."), 200);
            
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()));
        }
    }
    
    public function createUsername_post() {
        try{
            Helper::rawInput();
            $userData = $this->authorization_token->userData();
            $this->form_validation->set_rules('firstName', 'First Name', 'trim|required');
            $this->form_validation->set_rules('lastName', 'Last Name', 'trim|required');
            $this->form_validation->set_rules('userName', 'User Name', 'trim|required|is_unique[users.userName]|alpha_numeric');
            if ($this->form_validation->run() === false) {
                $this->response(Helper::error($this->form_validation->error_array()));
                die;
            }
            $data = [
                'userName' => $this->input->post('userName'),
                'firstName' => $this->input->post('firstName'),
                'lastName' => $this->input->post('lastName')
            ];
            
            $result = $this->User_model->updateProfile($userData->user_id, $data);
            
            $userData = $this->User_model->check_userId_exists($userData->user_id, true);
            $userData = Helper::userCollection( (array)$userData );
            if (isset($userData['password'])) {
                unset($userData['password']);
            }
            
            $this->response(Helper::success("Profile has updated.",$userData), 200);
            
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()));
        }
    }
    
    public function notifications_get(){
        try{
            $this->load->model('Push_notification_model');
            Helper::rawInput();
            $userData = $this->authorization_token->userData();
            $data = $this->Push_notification_model->get(['userId' => $userData->user_id ]);
            foreach( $data as $key => $res ){
                $data[$key]['is_read'] = (int)$res['is_read'];
            }
            $this->response(Helper::success("ok", $data), 200);
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()));
        }
    }
    
    public function get_user_post( ){
        try{
            Helper::rawInput();
            $this->authorization_token->userData();
            $user_id = $this->input->post('user_id');
            $this->load->model('Sesh_review_model');
            if( !$user_id ) {
                throw new Exception("Please prodive user ID.");
            }
            $userData = $this->User_model->check_userId_exists($user_id, true);
            $userData = Helper::userCollection( (array)$userData );
            // print_r($userData);
            if (isset($userData['password'])) {
                unset($userData['password']);
            }
            $userData['reviews'] = $this->Sesh_review_model->getReviews(['userId' => $user_id ]);
            $reviewer = [];
            foreach( $userData['reviews'] as $key => $user ){
                $user = Helper::reviewCollection( (array) $user);
                $ReUserData = (array)$this->User_model->check_userId_exists( $user['reviewer_id'], true);
                $user['reviewer'] = Helper::userCollection($ReUserData);
                if (isset($user['reviewer']['password'])) {
                    unset($user['reviewer']['password']);
                }
                $reviewer[] = $user;
            }
            
            $userData['reviews'] = $reviewer;
            $this->response(Helper::success("ok", $userData), 200);
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()));
        }
    }

    public function close_account_post( ){
        try{
            Helper::rawInput();
            $userData = $this->authorization_token->userData();
            $this->load->model('Sesh_model');
            $this->load->model('Friend_model');
            $this->load->model('Sesh_user_model');
            $this->load->model('User_device_model');
            $user_id = $userData->user_id;
            $userData = $this->User_model->check_userId_exists($user_id, true);
            if( $userData->firstName == "" ) {
                $userData->firstName = $userData->email;
            }
            //print_r($userData);die;
            //Remove friends
            $this->Friend_model->delete_friends_belongsTo($user_id);

            //Cancel seshs for current user
            $dataForSeshes = $this->Sesh_model->getSeshByUser( $user_id, [], ["status"=> Sesh_status::LIVE ] );
            foreach( $dataForSeshes as $seshObj ) {
                $seshObj = Helper::SeshCollection($seshObj);
                $this->Sesh_model->endSesh(  $seshObj['id'], Sesh_status::CANCELED );
                $this->Sesh_user_model->UpdateSeshByUserId( $seshObj['id'] , $user_id, Sesh_boddy_status::END );
                $sesh_users = $this->Sesh_user_model->getSeshUsers( $seshObj['id'] );

                $userIds = [];
                foreach ($sesh_users as $single) {
                    if( $single['user_id'] != $seshObj['userId'] ) {
                        $userIds[]= $single['user_id'];
                    }   
                }
                
                 $number_of_existing_user = $this->User_model->check_userIds_exists( $userIds, true );
            
                $finalUsers =[];
                foreach( $number_of_existing_user as $user ){
                     $_user = Helper::userCollection( $user );
                     unset($_user['password']);
                     unset($_user['platform']);
                     $finalUsers[] = $_user;
                }
            
                $data = [];
                $data['sesh_id'] = $seshObj['id'];
                $data['type'] = $seshObj['type'];
                $data['action'] = SESH_ACTION::CANCELED; 
                $data['members'] = $finalUsers;
                if( count( $userIds ) )
                $this->User_device_model->sendNotifications( $userIds, "Sesh has been canceled by ".$userData->firstName."." ,$data , $data['type'] );
            }

            //End Sesh for current user
            $listOfAcceptedOrPendingSeshes = $this->Sesh_user_model->getSeshBy( [ "user_id" => $user_id , "in_status" => [ Sesh_boddy_status::ACCEPTED ] ] );
//            print_r($listOfAcceptedOrPendingSeshes);die;
            foreach( $listOfAcceptedOrPendingSeshes as $invitedSesh ){
                $seshObj = $this->Sesh_model->get_detail(["id" =>   $invitedSesh['sesh_id'] ]);
                $this->Sesh_user_model->UpdateSeshByUserId( $seshObj->id , $user_id, Sesh_boddy_status::END );
                $getSeshes = $this->Sesh_user_model->getSeshBy( [ "sesh_id" => $seshObj->id , "in_status" => [ Sesh_boddy_status::ACCEPTED, Sesh_boddy_status::PENDING ] ] );
                if( !count( $getSeshes ) ){
                    $this->Sesh_model->endSesh(  $seshObj->id, Sesh_status::END );
                }

                $sesh_users = $this->Sesh_user_model->getSeshUsers( $seshObj->id );
//                print_r($sesh_users);die;
                $userIds = [];
                foreach ($sesh_users as $single) {
                    if( $single['user_id'] != $user_id ) {
                        $userIds[]= $single['user_id'];
                    }   
                }
                
                $number_of_existing_user = $this->User_model->check_userIds_exists( $userIds, true );
                $finalUsers =[];
                foreach( $number_of_existing_user as $user ){
                    $_user = Helper::userCollection( $user );
                    unset($_user['password']);
                    unset($_user['platform']);
                    $finalUsers[] = $_user;
                }
                
                $data = [];
                $data['sesh_id'] = $seshObj->id;
                $data['type'] = $seshObj->type;
                $data['members'] = $finalUsers;
                $data['action'] = SESH_ACTION::ENDED; 
                if( count( $userIds ) ){
                    $this->User_device_model->sendNotifications( $userIds, "Sesh has been ended by ".$userData->firstName."." ,$data , $data['type'] );
                }
            }

            //Reject Sesh for current user
            $listOfAcceptedOrPendingSeshes = $this->Sesh_user_model->getSeshBy( [ "user_id" => $user_id , "in_status" => [ Sesh_boddy_status::PENDING ] ] );
            foreach( $listOfAcceptedOrPendingSeshes as $invitedSesh ){
                $seshObj = $this->Sesh_model->get_detail(["id" =>   $invitedSesh['sesh_id'] ]);
                $this->Sesh_user_model->UpdateSeshByUserId( $invitedSesh['sesh_id'] , $user_id, Sesh_boddy_status::REJECTED );
                $getSeshes = $this->Sesh_user_model->getSeshBy( [ "sesh_id" => $seshObj->id, "in_status" => [ Sesh_boddy_status::ACCEPTED, Sesh_boddy_status::PENDING ] ] );
                if( !count( $getSeshes ) ){
                    $this->Sesh_model->endSesh( $seshObj->id , Sesh_status::END );
                }

                $sesh_users = $this->Sesh_user_model->getSeshUsers( $seshObj->id );

                $userIds = [];
                foreach ($sesh_users as $single) {
                    if( $single['user_id'] != $user_id ) {
                        $userIds[]= $single['user_id'];
                    }   
                }
                
                $number_of_existing_user = $this->User_model->check_userIds_exists( $userIds, true );
                $finalUsers =[];
                foreach( $number_of_existing_user as $user ){
                    $_user = Helper::userCollection( $user );
                    unset($_user['password']);
                    unset($_user['platform']);
                    $finalUsers[] = $_user;
                }
                
                $data = [];
                $data['sesh_id'] = $seshObj->id;
                $data['type'] = $seshObj->type;
                $data['members'] = $finalUsers;
                $data['action'] = SESH_ACTION::REJECTED; 
                if( count( $userIds ) ){
                    $this->User_device_model->sendNotifications( $userIds, "Sesh has been ended by ".$userData->firstName."." ,$data , $data['type'] );
                }
            }
            
            //delete fcm and device token
            $this->User_device_model->deleteDevice(['userId' => $user_id]);
            
            $result = $this->User_model->updateProfile($user_id, array(
                'userName'=> "", 
                'is_deleted' => 1, 
                'age_verified' => 0, 
                'deleted_at'=> date('Y-m-d H:i:s'),
                'userStatus' => 0
            ));
            $this->response(Helper::success("The Account has closed."), 200);
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()));
        }
    }
}
