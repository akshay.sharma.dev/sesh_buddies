<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');

header('Access-Control-Allow-Origin:*');
header('Content-Type: application/json');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Friend extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('User_model');
        $this->load->model('Friend_model');
        $this->load->helper('common-utility');
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->library('Authorization_Token');
    }

    private function validEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /*
     * Add new friends
     * @param email IDs
     */
    public function add_post()
    {
        try {
            Helper::rawInput();
            $userData = $this->authorization_token->userData();
            if (empty($this->input->post('friend_ids')) || !is_array($this->input->post('friend_ids'))) {
                $this->response(Helper::error("Please provide friend IDs"), 200);
                die;
            }

            if ($this->Friend_model->add_friends($userData->user_id, $this->input->post('friend_ids'))) {
                $this->response(Helper::success("friends added successfully."), 200);
            } else {
                $this->response(Helper::error("Something went wrong."), 200);
                die;
            }
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()), 200);
        }
    }

    public function remove_post()
    {
        try {
            Helper::rawInput();
            $userData = $this->authorization_token->userData();
            if (empty($this->input->post('friend_ids')) || !is_array($this->input->post('friend_ids'))) {
                $this->response(Helper::error("Please provide friend IDs"), 200);
                die;
            }

            if ($friends_exist = $this->Friend_model->get_remove_friends_data($userData->user_id, $this->input->post('friend_ids'))) {

                $friends_ids = array_column($friends_exist, 'id');
                $records_deleted =  $this->Friend_model->delete_friend($userData->user_id, $friends_ids);
                if ($records_deleted !=false && count($friends_ids) == $records_deleted) {
                    $this->response(Helper::success("friends removed successfully."), 200);
                    die;
                } else {
                    $this->response(Helper::error("Something went wrong."), 200);
                    die;
                }   
            } else {
                $this->response(Helper::error("Something went wrong."), 200);
                die;
            }
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()), 200);
        }
    }

    /*
     * get friends
     * @param email IDs
     */
    public function get_get()
    {
        try {
            Helper::rawInput();
            $userData = $this->authorization_token->userData();
            $users = $this->Friend_model->get_friends($userData->user_id);
            foreach ($users as $key => $user) {
                $users[$key] = Helper::userCollection($user);
            }
            $this->response(Helper::success("", $users));
            die;
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()), 200);
            die;
        }
    }

    /*
     * get friends
     * @param email IDs
     */

    public function buddies_post()
    {
        try {
            Helper::rawInput();
            $this->form_validation->set_rules('data_type', 'Data type', 'trim|required');
            $final_users = [];
            $userData = $this->authorization_token->userData();

            $emailIds = $this->input->post('emailIds');

            if (count($emailIds)) {
                $user_ids_from_email  = [];
                $users = $this->User_model->findUserByEmail($emailIds);
                $user_ids = [];
                foreach ($users as $user) {
                    $user_ids_from_email[] = $user['userId'];
                }
                $friends = $this->Friend_model->get_friends_by_ids($userData->user_id, $user_ids_from_email, 'id, friend_id');
                $friend_ids = [];
                foreach ($friends as $friend) {
                    $friend_ids[] = $friend['friend_id'];
                }

                foreach ($user_ids_from_email as $user_id) {
                    if (!in_array($user_id, $friend_ids)) {
                        $user_ids[] = $user_id;
                    }
                }

                if (count($user_ids)) {
                    $this->db->select('userId, firstName, lastName, userName, email, coverPic, profilePic, userStatus, gender,quantity,quality,rolls');
                    $this->db->where_in('userId', $user_ids);
                    $query = $this->db->get('users');
                    $final_users = $query->result_array();
                    foreach ($final_users as $key => $user) {
                        $final_users[$key] = Helper::userCollection($user);
                    }
                }
            } else {
                $user_ids = [$userData->user_id];
                $friends = $this->Friend_model->get_friends($userData->user_id, 'id, friend_id', false);
                foreach ($friends as $friend) {
                    $user_ids[] = $friend['friend_id'];
                }

                if (count($user_ids)) {
                    $this->db->select('userId, firstName, lastName, userName, email, coverPic, profilePic, userStatus, gender,quantity,quality,rolls');
                    $this->db->where_not_in('userId', $user_ids);
                    $query = $this->db->get('users');
                    $final_users = $query->result_array();
                    foreach ($final_users as $key => $user) {
                        $final_users[$key] = Helper::userCollection($user);
                    }
                }
            }

            $this->response(Helper::success("", $final_users));
            die;
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()), 200);
            die;
        }
    }
}
