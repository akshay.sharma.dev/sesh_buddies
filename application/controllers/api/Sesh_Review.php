<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

header('Access-Control-Allow-Origin:*');
header('Content-Type: application/json');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Sesh_Review extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('User_model');
        $this->load->model('Sesh_model');
        $this->load->model('Sesh_user_model');
        $this->load->model('Sesh_review_model');
        
        $this->load->library('encryption');
//        $this->load->helper('string');
        $this->load->helper('common-utility');
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->library('Authorization_Token');
    }
    
    public function add_post(){
        try{
            Helper::rawInput();
            $userData = $this->authorization_token->userData();
            
            $this->form_validation->set_rules('sesh_id', 'Sesh ID', 'trim|required');
            $this->form_validation->set_rules('reviews[]', 'Reviews', 'required');
  
            if ($this->form_validation->run() === false) {
                $this->response(Helper::error($this->form_validation->error_array()), 200);
                die;
            }
            
            $userids = [];
            
            $sesh_data = $this->input->post();
            $seshObj = $this->Sesh_model->get_detail(["id" =>  $sesh_data['sesh_id'] ]);
            
            if( empty ( $seshObj ) ) {
                throw new Exception("Please provide valid Sesh ID");
            }
            
            $data_saved = [];
            foreach ( $sesh_data['reviews'] as $review ) {
                $userids[] = $review['userId'];
                if( !isset($review['userId']) && empty( $review['userId'] ) ){
                    throw new Exception("Please Provide User ID for each Review");
                }
                
                if( !isset($review['quantity']) && empty( $review['quantity'] ) ){
                    throw new Exception("Please Provide quantity for each Review");
                }
                
                if( !isset($review['quality']) && empty( $review['quality'] ) ){
                    throw new Exception("Please Provide quality for each Review");
                }
                
                if( !isset($review['rolls']) && empty( $review['rolls'] ) ){
                    throw new Exception("Please Provide rolls for each Review");
                }
                
                $data = $review;
                $data['reviewer_id'] = $userData->user_id;
                $data['sesh_id'] = $sesh_data['sesh_id'];
                
                $data_saved[] = $data;
            }
            
            if( count( $data_saved ) ) {
                $this->Sesh_review_model->save_review( $data_saved );
            }
            
            $this->Sesh_user_model->UpdateSeshByUserId( $sesh_data['sesh_id'] , $userData->user_id, Sesh_boddy_status::END );
            $getSeshes = $this->Sesh_user_model->getSeshBy( [ "sesh_id" => $sesh_data['sesh_id'] , "in_status" => [ Sesh_boddy_status::ACCEPTED, Sesh_boddy_status::PENDING ] ] );
            if( !count( $getSeshes ) ){
                $this->Sesh_model->endSesh( $sesh_data['sesh_id'] , Sesh_status::END );
            }
            foreach( $userids as $single_user_id ){
                $avg_result = $this->Sesh_review_model->getReviewsAvg( $single_user_id );
                $this->User_model->updateProfile( $single_user_id , (array)$avg_result );
            }
           
            $this->response(Helper::success("",$data_saved), 200);
            die;
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()), 200);
            die;
        }
    }
    
    public function get_get( $sesh_id=null ){
        try{
            $userData = $this->authorization_token->userData();
            $data = [ 'sesh_id' => $sesh_id];
            $data['reviewer_id'] = $userData->user_id;
            $data_result = $this->Sesh_review_model->getSeshReviewByUser( $data );
            $this->response(Helper::success("ok",$data_result), 200);
            die;
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()), 200);
            die;
        }
    }
    
    public function by_user_post(  ){
        try{
            Helper::rawInput();
            $sesh_data = $this->input->post();
            
            $data_result = $this->Sesh_review_model->getReviews( $sesh_data );
            $currentUser =[];
            if( isset( $sesh_data['userId'] ) ){
                $currentUser = $this->User_model->current_user($sesh_data['userId'] , "quantity, quality, rolls" );
                foreach ( $currentUser as $key => $value ){
                    $currentUser[$key] = floatval($value);
                }
            }
            foreach( $data_result as $key => $review ){
                $reviwer = $this->User_model->current_user($review['reviewer_id'] , "lastName, userName, profilePic, userStatus" );
                $review['quantity'] = floatval($review['quantity']);
                $review['quality'] = floatval($review['quality']);
                $review['rolls'] = floatval($review['rolls']);
                $review['lastName'] = $reviwer['lastName'];
                $review['userName'] = $reviwer['userName'];
                $review['profilePic'] = $reviwer['profilePic'];
                $review['userStatus'] = (int) $reviwer['userStatus'];
                $data_result[$key] = $review;
            }
            $currentUser['list'] = $data_result;
            $this->response(Helper::success("ok",$currentUser), 200);
            die;
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()), 200);
            die;
        }
    }
}