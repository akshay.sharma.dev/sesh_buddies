<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

header('Access-Control-Allow-Origin:*');
header('Content-Type: application/json');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Sesh extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('User_model');
        $this->load->model('User_device_model');
        $this->load->model('Sesh_model');
        
        $this->load->library('encryption');
        $this->load->helper('string');
        $this->load->helper('common-utility');
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->library('Authorization_Token');
        $this->load->model('Sesh_user_model');
    }
    
    public function add_post(){
        try{
            Helper::rawInput();
            $userData = $this->authorization_token->userData();
            $userId=$userData->user_id;
            $this->form_validation->set_rules('type', 'Sesh type', 'trim|required');
            //            $this->form_validation->set_rules('point', 'Point', 'trim|required');
            //            $this->form_validation->set_rules('strain', 'Strain', 'trim|required');
            $this->form_validation->set_rules('time', 'Time', 'trim|required');
            $this->form_validation->set_rules('date', 'Date', 'trim|required');
            $this->form_validation->set_rules('location', 'Location', 'trim|required');
            $this->form_validation->set_rules('utensils', 'Utensils', 'trim|required');
            $this->form_validation->set_rules('buddies', 'Buddies', 'trim');
            $this->form_validation->set_rules('flavour', 'Falvour', 'trim');
            
            $buddies_array = array_filter( $this->input->post('buddies') );
            if( !is_array( $buddies_array ) ){
                throw new Exception("Please provide array of buddies");
            }
            if( !is_array( $buddies_array ) || !count( $buddies_array ) ){
                throw new Exception("Please provide atleast one buddy.");
            }
            
            if ($this->form_validation->run() === false) {
                $this->response(Helper::error($this->form_validation->error_array()), 200);
                die;
            }
            
            $startDate = date('Y-m-d H:i:s', strtotime("-30 days") );
//                $endDate = date('Y-m-d H:i:s');
            $dataForSeshes = $this->Sesh_model->getSeshByUser( $userId, [] , [ 'type' => "SMO",  'status' => Sesh_status::LIVE , 'created_at' => $startDate ] );
            
            if(count( $dataForSeshes ) >= 5){
                throw new Exception("You can't create more then 5 sesh within 30 days.");
            }
            
            $number_of_existing_user = $this->User_model->check_userIds_exists( $buddies_array, true );
            
            if( count( $buddies_array ) !== count($number_of_existing_user) ){
                throw new Exception("Please provide valid buddies IDs");
            }
            
            $data = $this->input->post();
            
            $finalUsers =[];
            $number_of_existing_user[] = $this->User_model->current_user( $userData->user_id );
            foreach( $number_of_existing_user as $user ){
                 $_user = Helper::userCollection( $user );
                 unset($_user['password']);
                 unset($_user['platform']);
                 $finalUsers[] = $_user;
            }
            
            $data['userId'] = $userData->user_id;
            $data['members'] = $finalUsers;
            $data['status'] = Sesh_status::LIVE;
            
            $sesh_id = $this->Sesh_model->save_sesh_and_assign_buddies( $data );
            $data['sesh_id'] = $sesh_id;
            $data['action'] = SESH_ACTION::INVITED;
            
            $this->User_device_model->sendNotifications( $buddies_array, "You’ve been invited to a ". strtoupper($this->input->post('type')) ." sesh." ,$data , $data['type'] );
            
            unset($data['action']);
            $this->response(Helper::success("Sesh created successfully",$data), 200);
            die;
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()), 200);
            die;
        }
    }
    
    public function get_post( $type=null ){
        try{
            Helper::rawInput();
            $this->form_validation->set_rules('status', 'Sesh status', 'trim|in_list[0,1,2]');
            
            if ($this->form_validation->run() === false) {
                $this->response(Helper::error($this->form_validation->error_array()), 200);
                die;
            }
            
            $userData = $this->authorization_token->userData();
            $user_id = $userData->user_id;
            $userData = $this->User_model->check_userId_exists($user_id, true);
            
            $status = $this->input->post('status');
            
            $getSeshes = $this->Sesh_user_model->getSeshBy( [ "user_id"=> $user_id ] );
            
            $getSeshesIds = [];
            foreach ($getSeshes as $sesh){
                $getSeshesIds[] = $sesh['sesh_id'];
            }
            
            if( !is_numeric($status) ){
                $dataForSeshes = $this->Sesh_model->getSeshByUser( $user_id, $getSeshesIds, [ 'type' => $type, 'created_at' => $userData->deleted_at ] );
            } else {
                $dataForSeshes = $this->Sesh_model->getSeshByUser( $user_id, $getSeshesIds, [ 'type' => $type, 'status' => $status, 'created_at' => $userData->deleted_at ] );
            }
            
            $finalSeshList = [];
            foreach ( $dataForSeshes as $key => $sesh ) {
                $sesh = Helper::SeshCollection($sesh);
                $getSeshes_users = $this->Sesh_user_model->getSeshBy( 
                    [ "sesh_id" => $sesh['id'] ], true
                );
                
                $finalMembers = [];
                
                foreach ( $getSeshes_users as $key => $user ){
                    if( isset( $user['password'] ) ){
                        unset( $user['password'] );
                    }
                    $userObj = Helper::userCollection( $this->User_model->getAllowedFields( $user ) );
                    $userObj['sesh_status'] = (int) $user['status'];
                    $finalMembers[] = $userObj;
                }
                $sesh['members'] = $finalMembers;
                $finalSeshList[] = $sesh;
            }
            
            $this->response(Helper::success("ok",$finalSeshList), 200);
            die;
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()), 200);
            die;
        }
    }
    
    public function detail_post(){
        try{
            Helper::rawInput();
            $this->form_validation->set_rules('id', 'Sesh ID', 'trim|required');

            if ($this->form_validation->run() === false) {
                $this->response(Helper::error($this->form_validation->error_array()), 200);
                die;
            }
            $userData = $this->authorization_token->userData();
            
            $seshObj = $this->Sesh_model->get_detail( ["id" => $this->input->post('id') ] );
            
            if( empty ( $seshObj ) ) {
                throw new Exception("Please provide valid Sesh ID");
            }
            
            $sesh = Helper::SeshCollection((array) $seshObj);
            $getSeshes_users = $this->Sesh_user_model->getSeshBy( 
                [ "sesh_id" => $sesh['id'] ], true
            );

            $finalMembers = [];
              
            foreach ( $getSeshes_users as $key => $user ){
                if( isset( $user['password'] ) ){
                    unset( $user['password'] );
                }

                $userObj = Helper::userCollection( $this->User_model->getAllowedFields( $user ) );
                $userObj['sesh_status'] = (int) $user['status'];
                $finalMembers[] = $userObj;

            }
            //                $finalSeshList[] = $sesh;
            //                print_r($getSeshes_users);die;
            $sesh['members'] = $finalMembers;
            
            $this->response(Helper::success("ok",$sesh), 200);
            die;
            
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()), 200);
            die;
        }
    }
    
    public function get_all_get( $type=null ){
        try{
            $userData = $this->authorization_token->userData();
            $data = $this->Sesh_model->all( $type, $userData );
            $this->response(Helper::success("ok",$data), 200);
            die;
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()), 200);
            die;
        }
    }
    
    public function accept_post(){
        try{
            Helper::rawInput();
            $this->form_validation->set_rules('id', 'Sesh ID', 'trim|required');
            
            if ($this->form_validation->run() === false) {
                $this->response(Helper::error($this->form_validation->error_array()), 200);
                die;
            }
            
            $userData = $this->authorization_token->userData();
            //            print_r($userData);die;
            $seshObj = $this->Sesh_model->get_detail(["id" =>  $this->input->post('id') ]);
            if( empty ( $seshObj ) ) {
                throw new Exception("Please provide valid Sesh ID");
            }
            
            $sesh_users = $this->Sesh_user_model->getSeshUsers( $seshObj->id );

            $userIds = [];
            foreach ($sesh_users as $single) {
                if( $single['user_id'] != $userData->user_id ) {
                    $userIds[]= $single['user_id'];
                }   
            }
            
            $number_of_existing_user = $this->User_model->check_userIds_exists( $userIds, true );
            
            $finalUsers =[];
            foreach( $number_of_existing_user as $user ){
                 $_user = Helper::userCollection( $user );
                 unset($_user['password']);
                 unset($_user['platform']);
                 $finalUsers[] = $_user;
            }
            
            $data = [];
            $data['sesh_id'] = $seshObj->id;
            $data['type'] = $seshObj->type;
            $data['members'] = $finalUsers;
            $data['action'] = SESH_ACTION::ACCEPTED; 
            if( count( $userIds ) ){
                $this->User_device_model->sendNotifications( $userIds, strtoupper($data['type']) . " sesh has been accepted." ,$data , $data['type'] );
            }
            
            $this->Sesh_user_model->UpdateSeshByUserId( $this->input->post('id'), $userData->user_id, Sesh_boddy_status::ACCEPTED );
            $this->response(Helper::success("Sesh has accepted."), 200);
            die;
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()), 200);
            die;
        }
    }

    /**
     * @param Token
     * @param seshID
     * @description End Sesh After Accept
     */
    public function end_post(){
        try{
            Helper::rawInput();
            $this->form_validation->set_rules('id', 'Sesh ID', 'trim|required');
            
            if ($this->form_validation->run() === false) {
                $this->response(Helper::error($this->form_validation->error_array()), 200);
                die;
            }
            
            $userData = $this->authorization_token->userData();
           
            $seshObj = $this->Sesh_model->get_detail(["id" =>  $this->input->post('id') ]);
            if( empty ( $seshObj ) ) {
                throw new Exception("Please provide valid Sesh ID");
            }
            
            $this->Sesh_user_model->UpdateSeshByUserId( $this->input->post('id'), $userData->user_id, Sesh_boddy_status::END );
            $getSeshes = $this->Sesh_user_model->getSeshBy( [ "sesh_id" => $this->input->post('id') , "in_status" => [ Sesh_boddy_status::ACCEPTED, Sesh_boddy_status::PENDING ] ] );
            if( !count( $getSeshes ) ){
                $this->Sesh_model->endSesh(  $this->input->post('id'), Sesh_status::END );
            }
            
            $sesh_users = $this->Sesh_user_model->getSeshUsers( $seshObj->id );

            $userIds = [];
            foreach ($sesh_users as $single) {
                if( $single['user_id'] != $userData->user_id ) {
                    $userIds[]= $single['user_id'];
                }   
            }
            
            $number_of_existing_user = $this->User_model->check_userIds_exists( $userIds, true );
            
            $finalUsers =[];
            foreach( $number_of_existing_user as $user ){
                 $_user = Helper::userCollection( $user );
                 unset($_user['password']);
                 unset($_user['platform']);
                 $finalUsers[] = $_user;
            }
            
            $data = [];
            $data['sesh_id'] = $seshObj->id;
            $data['type'] = $seshObj->type;
            $data['members'] = $finalUsers;
            $data['action'] = SESH_ACTION::ENDED; 
            if( count( $userIds ) ){
                $this->User_device_model->sendNotifications( $userIds, strtoupper( $data['type'] )." sesh has been ended." ,$data , $data['type'] );
            }
            
            $this->response(Helper::success("ok"), 200);
            die;
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()), 200);
            die;
        }
    }
    
    /**
     * @param Token
     * @param seshID
     * @description Reject Sesh before Accept
     */
    public function reject_post(){
        try{
            Helper::rawInput();
            $this->form_validation->set_rules('id', 'Sesh ID', 'trim|required');
            
            if ($this->form_validation->run() === false) {
                $this->response(Helper::error($this->form_validation->error_array()), 200);
                die;
            }
            
            $userData = $this->authorization_token->userData();
           
            $seshObj = $this->Sesh_model->get_detail(["id" =>  $this->input->post('id') ]);
            if( empty ( $seshObj ) ) {
                throw new Exception("Please provide valid Sesh ID");
            }
            
            $this->Sesh_user_model->UpdateSeshByUserId( $this->input->post('id'), $userData->user_id, Sesh_boddy_status::REJECTED );
            $getSeshes = $this->Sesh_user_model->getSeshBy( [ "sesh_id" => $this->input->post('id') , "in_status" => [ Sesh_boddy_status::ACCEPTED, Sesh_boddy_status::PENDING ] ] );
            if( !count( $getSeshes ) ){
                $this->Sesh_model->endSesh(  $this->input->post('id'), Sesh_status::END );
            }
            
            $sesh_users = $this->Sesh_user_model->getSeshUsers( $seshObj->id );

            $userIds = [];
            foreach ($sesh_users as $single) {
                if( $single['user_id'] != $userData->user_id ) {
                    $userIds[]= $single['user_id'];
                }   
            }
            
            $number_of_existing_user = $this->User_model->check_userIds_exists( $userIds, true );
            
            $finalUsers =[];
            foreach( $number_of_existing_user as $user ){
                 $_user = Helper::userCollection( $user );
                 unset($_user['password']);
                 unset($_user['platform']);
                 $finalUsers[] = $_user;
            }
            
            $data = [];
            $data['sesh_id'] = $seshObj->id;
            $data['type'] = $seshObj->type;
            $data['action'] = SESH_ACTION::REJECTED; 
            $data['members'] = $finalUsers;
            if( count( $userIds ) ){
                $this->User_device_model->sendNotifications( $userIds, strtoupper( $data['type'] )." sesh has been rejcted."  ,$data , $data['type'] );
            }
            
            $this->response(Helper::success("Sesh request rejected"), 200);
            die;
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()), 200);
            die;
        }
    }
    /**
     * @param Token
     * @param seshID
     * @description Only Sesh Owner can cancel the sesh 
     */
    public function cancel_post(){
        try{
            Helper::rawInput();
            $this->form_validation->set_rules('id', 'Sesh ID', 'trim|required');
            
            if ($this->form_validation->run() === false) {
                $this->response(Helper::error($this->form_validation->error_array()), 200);
                die;
            }
            
            $userData = $this->authorization_token->userData();
           
            $seshObj = $this->Sesh_model->get_detail(["id" =>  $this->input->post('id') ]);
//            print_r($seshObj);die;
            if( empty ( $seshObj ) ) {
                throw new Exception("Please provide valid Sesh ID");
            }
            
            if( $seshObj->userId == $userData->user_id ){
                $this->Sesh_model->endSesh(  $this->input->post('id'), Sesh_status::CANCELED );
                $sesh_users = $this->Sesh_user_model->getSeshUsers( $seshObj->id );
//                print_r($sesh_users);
//                die;
                $userIds = [];
                foreach ($sesh_users as $single) {
                    if( $single['user_id'] != $seshObj->userId ) {
                        $userIds[]= $single['user_id'];
                    }   
                }
                
                 $number_of_existing_user = $this->User_model->check_userIds_exists( $userIds, true );
            
                $finalUsers =[];
                foreach( $number_of_existing_user as $user ){
                     $_user = Helper::userCollection( $user );
                     unset($_user['password']);
                     unset($_user['platform']);
                     $finalUsers[] = $_user;
                }
            
                $data = [];
                $data['sesh_id'] = $seshObj->id;
                $data['type'] = $seshObj->type;
                $data['action'] = SESH_ACTION::CANCELED; 
                $data['members'] = $finalUsers;
                if( count( $userIds ) )
                $this->User_device_model->sendNotifications( $userIds, strtoupper( $data['type'] )." sesh has been canceled." ,$data , $data['type'] );
                
            } else {
                throw new Exception("Only sesh owner can cancel the sesh.");
            }
            
            $this->response(Helper::success("Sesh has been canceled."), 200);
            die;
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()), 200);
            die;
        }
    }
}