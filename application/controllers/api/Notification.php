<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

header('Access-Control-Allow-Origin:*');
header('Content-Type: application/json');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Notification extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->helper('common-utility');
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->library('Authorization_Token');
        $this->load->model('Notification_model');
    }
    
    public function get_settings_get() {
        try{
            $userData = $this->authorization_token->userData();
            $settings = $this->Notification_model->getNotificationSetting( $userData->user_id );
            $this->response( Helper::success( "", $settings), 200 );
            die;
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()), 200);
            die;
        }
    }

    public function update_settings_post() {
        try{
            Helper::rawInput();
            $userData = $this->authorization_token->userData();
            $this->form_validation->set_rules('shmoke', 'shmoke', 'trim|in_list[0,1]');
            $this->form_validation->set_rules('match', 'match', 'trim|in_list[0,1]');
            $this->form_validation->set_rules('drop', 'drop', 'trim|in_list[0,1]');
            $this->form_validation->set_rules('smo', 'smo', 'trim|in_list[0,1]');
            $this->form_validation->set_rules('deals', 'deals', 'trim|in_list[0,1]');
            $this->form_validation->set_rules('smo_iou', 'SMO IOU', 'trim|in_list[0,1]');
            
            if ($this->form_validation->run() === false) {
                $this->response(Helper::error($this->form_validation->error_array()), 200);
                die;
            }
            
            $counter = $this->Notification_model->getSettingsByUser( $userData->user_id );
            if( $counter ){
                $result = $this->Notification_model->updateNotificationSetting( $userData->user_id, $this->input->post() );
                if( !$result ) {
                    $this->response(Helper::error("Something went wrong"), 200);
                    die;
                }
                
            } else {
                $data = $this->input->post();
                $data['userId'] = $userData->user_id;
                $result = $this->Notification_model->saveSettings( $data );
                if( !$result ) {
                    $this->response(Helper::error("Something went wrong"), 200);
                    die;
                }
            }
            
            $this->response(Helper::success("Notification settings has saved"), 200);
            die;
        } catch (Exception $ex) {
            $this->response(Helper::error($ex->getMessage()), 200);
            die;
        }
        
    }
    
}
