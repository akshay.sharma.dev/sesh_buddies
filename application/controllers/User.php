<?php

defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin:*');
header('Content-Type: application/json');

class User extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('User_model');
        $this->load->library('encryption');
        $this->load->helper('string');

        $this->load->database();
    }

    public function convert_arrayvalue_nullto_emptystring($array) {
        foreach ($array as $key => $value) {
            if (is_null(trim($value))) {
                $array[$key] = "";
            }
        }
        return $array;
    }

    public function trim_data($array) {
        foreach ($array as $key => $value) {
            if (is_null($value)) {
                $array[$key] = "";
            } else {
                $array[$key] = trim($value);
            }
        }
        return $array;
    }

    public function imgAbsPath($data) {
        if (isset($data->profilePic) && !empty(trim($data->profilePic))) {
            //$data->profilePic = base_url().'assets/uploads/profile_pic/'.$data->profilePic;
            $data->profilePic = $data->profilePic;
        }
        if (isset($data->coverPic) && !empty(trim($data->coverPic))) {
            //$data->coverPic = base_url().'assets/uploads/cover_pic/'.$data->coverPic;
            $data->coverPic = $data->coverPic;
        }
        return $data;
    }

    public function getHeadersData() {
        $accessToken = $userId = $platform = "";
        $headers = getallheaders();
        if (isset($headers['Userid']) && !empty($headers['Userid'])) {
            $userId = $headers['Userid'];
        }
        if (isset($headers['Accesstoken']) && !empty($headers['Accesstoken'])) {
            $accessToken = $headers['Accesstoken'];
        }
        if (isset($headers['Platform']) && $headers['Platform'] != "") {
            $platform = $headers['Platform'];
        }
        return ['userId' => $userId, 'accessToken' => $accessToken, 'platform' => $platform];
    }

    function convertRawToArrayPost() {
        $posted_data = (array) json_decode(file_get_contents('php://input'), TRUE);
        return $posted_data;
    }

    public function register() {
        $postData = $this->convertRawToArrayPost();

        if (!isset($postData['email'])) {
            $email = "";
        } else {
            $email = $postData['email'];
        }
        if (!isset($postData['password'])) {
            $password = "";
        } else {
            $password = $postData['password'];
        }
        if (!isset($postData['loginType'])) {
            $loginType = "";
        } else {
            $loginType = $postData['loginType'];
        }
        if (!isset($postData['firstName'])) {
            $firstName = "";
        } else {
            $firstName = $postData['firstName'];
        }
        if (!isset($postData['lastName'])) {
            $lastName = "";
        } else {
            $lastName = $postData['lastName'];
        }
        if (!isset($postData['userName'])) {
            $userName = "";
        } else {
            $userName = $postData['userName'];
        }
        if (!isset($postData['fcmToken'])) {
            $fcmToken = "";
        } else {
            $fcmToken = $postData['fcmToken'];
        }

        $headers = $this->getHeadersData();
        $error_exist = 'no';
        $error_list = [];

        if (empty($firstName)) {
            $error_exist = 'yes';
            $error_list[] = 'firstName is required';
        }
        if (empty($lastName)) {
            $error_exist = 'yes';
            $error_list[] = 'lastName is required';
        }
        if (empty($userName)) {
            $userName = "";
        }
        if (empty($email)) {
            $error_exist = 'yes';
            $error_list[] = 'email is required';
        }
        if (empty($password)) {
            $error_exist = 'yes';
            $error_list[] = 'password is required';
        }
        if (empty($loginType)) {
            $error_exist = 'yes';
            $error_list[] = 'loginType is required';
        }
        if (empty($fcmToken)) {
            $fcmToken = '';
        }

        if (!empty($userName)) {
            $is_username_exists = $this->User_model->check_username_exists($userName);
            if ($is_username_exists['status'] == true) {
                $error_exist = 'yes';
                $error_list[] = 'Username is already exists.';
            }
        }
        $is_email_exists = $this->User_model->check_email_exists($email);

        if ($is_email_exists['status'] == true) {
            $error_exist = 'yes';
            $error_list[] = 'Email is already exists.';
        }

        if ($error_exist == 'no') {
            $accessToken = sha1($email . $userName . date('Y-m-d H:i:s'));
            $uid = sha1($email . $userName . date('Y-m-d'));
            ;
            $password = $this->encryption->encrypt($password);
            $data = array('uid' => $uid, 'firstName' => $firstName, 'lastName' => $lastName, 'userName' => $userName, 'email' => $email,
                'password' => $password, 'accessToken' => $accessToken, 'fcmToken' => $fcmToken, 'loginType' => $loginType);
            $result = $this->User_model->registration($this->trim_data($data));

            if (isset($result['user_data']->password)) {
                unset($result['user_data']->password);
            }
            if ($result['status'] == true) {
                $result['user_data']->userId = (int) $result['user_data']->userId;
                $result['user_data']->loginType = (int) $result['user_data']->loginType;
                if (!empty($result['user_data']->gender)) {
                    $result['user_data']->gender = (int) $result['user_data']->gender;
                } else {
                    $result['user_data']->gender = (int) 0;
                }
                $data = array("status" => 1, "errorMessage" => [], "message" => "user created successfully", 'data' => $result['user_data']);
            } else {
                $data = array("status" => 0, "errorMessage" => [], "message" => "Something went wrong.Please try again", 'data' => $result['user_data']);
            }
        } else {
            $data = array("status" => 0, "errorMessage" => $error_list, "message" => "", 'data' => (object) []);
        }
        $data['platform'] = (int) $headers['platform'];
        if (isset($data['data']->userStatus)) {
            $data['data']->userStatus = (int) $data['data']->userStatus;
        }
        echo json_encode($data);
    }

    public function editProfile() {
        $headers = $this->getHeadersData();
        $error_exist = 'no';
        $error_list = [];
        $postData = $this->convertRawToArrayPost();
        $param = [];
        if (!empty($postData)) {
            foreach ($postData as $key => $value) {
                if (!empty($value)) {
                    $param[$key] = $value;
                }
            }
        } else {
            $error_exist = 'yes';
            $error_list[] = 'No key data exists.';
        }

        if (isset($param['userName'])) {
            $is_username_exists = $this->User_model->check_username_exists_for_particular_user($param['userName'], $headers['userId']);
            if ($is_username_exists['status'] == true) {
                $error_exist = 'yes';
                $error_list[] = 'Username is already exists.';
            }
        }

        if ($error_exist == 'no') {
            $param['userId'] = $headers['userId'];
            $param['accessToken'] = $headers['accessToken'];
            $result = $this->User_model->editProfile($this->trim_data($param));
            if (isset($result['user_data']->password)) {
                unset($result['user_data']->password);
            }
            if ($result['status'] == true) {
                $result['user_data']->userId = (int) $result['user_data']->userId;
                $result['user_data']->loginType = (int) $result['user_data']->loginType;
                if (!empty($result['user_data']->gender)) {
                    $result['user_data']->gender = (int) $result['user_data']->gender;
                } else {
                    $result['user_data']->gender = (int) 0;
                }
                $data = array("status" => 1, "errorMessage" => [], "message" => "User Profile is updated successfully", 'data' => $result['user_data']);
            } else {
                $data = array("status" => 0, "errorMessage" => [], "message" => "Something went wrong.Please try again", 'data' => $result['user_data']);
            }
        } else {
            $data = array("status" => 0, "errorMessage" => $error_list, "message" => "", 'data' => (object) []);
        }
        $data['platform'] = (int) $headers['platform'];

        if (isset($data['data']->userStatus)) {
            $data['data']->userStatus = (int) $data['data']->userStatus;
        }
        echo json_encode($data);
    }

    public function login() {
        $postData = $this->convertRawToArrayPost();
        $headers = $this->getHeadersData();

        if (!isset($postData['email'])) {
            $email = "";
        } else {
            $email = $postData['email'];
        }
        if (!isset($postData['password'])) {
            $password = "";
        } else {
            $password = $postData['password'];
        }
        if (!isset($postData['loginType'])) {
            $loginType = "";
        } else {
            $loginType = $postData['loginType'];
        }
        # facebook parameter
        if (!isset($postData['firstName'])) {
            $firstName = "";
        } else {
            $firstName = $postData['firstName'];
        }
        if (!isset($postData['lastName'])) {
            $lastName = "";
        } else {
            $lastName = $postData['lastName'];
        }
        if (!isset($postData['userName'])) {
            $userName = "";
        } else {
            $userName = $postData['userName'];
        }
        if (!isset($postData['userId'])) {
            $userId = "";
        } else {
            $userId = $postData['userId'];
        }
        if (!isset($postData['profilePic'])) {
            $profilePic = "";
        } else {
            $profilePic = $postData['profilePic'];
        }
        if (!isset($postData['coverPic'])) {
            $coverPic = "";
        } else {
            $coverPic = $postData['coverPic'];
        }
        if (!isset($postData['accessToken'])) {
            $accessToken = "";
        } else {
            $accessToken = $postData['accessToken'];
        }
        if (!isset($postData['fcmToken'])) {
            $fcmToken = "";
        } else {
            $fcmToken = $postData['fcmToken'];
        }

        # facebook parameter


        if (empty($fcmToken)) {
            $fcmToken = '';
        }

        $error_exist = 'no';
        $error_list = [];

        if ($loginType == 1) { // login with FACEBOOK
            if (empty($email)) {
                $error_exist = 'yes';
                $error_list[] = 'email is required';
            }
            /* if( empty($userName) ) { $error_exist = 'yes'; $error_list[] = 'userName is required'; } */
        }
        if ($loginType == 2) { // login with SNAPCHAT
            $data = array("status" => 0, "errorMessage" => [], "message" => "Under construction", 'data' => []);
        }
        if ($loginType == 3) { // login with WITH_APP
            if (empty($email)) {
                $error_exist = 'yes';
                $error_list[] = 'email is required';
            }
            if (empty($password)) {
                $error_exist = 'yes';
                $error_list[] = 'password is required';
            }
        }

        if ($error_exist == 'no') {
            if ($loginType == 1) { // login with FACEBOOK
                $data = array('email' => $email, 'userName' => "", 'firstName' => $firstName, "lastName" => $lastName, 'userName' => $userName, 'profilePic' => $profilePic, 'coverPic' => $coverPic, 'fcmToken' => $fcmToken, 'loginType' => 1);

                $data = $this->convert_arrayvalue_nullto_emptystring($data);
                $result = $this->User_model->login_with_facebook($this->trim_data($data));
                if (isset($result['data']) && !empty($result['data'])) {
                    $result['data']->userId = (int) $result['data']->userId;
                    $result['data']->loginType = (int) $result['data']->loginType;
                    $result['data'] = $this->imgAbsPath($result['data']);
                    if (!empty($result['data']->gender)) {
                        $result['data']->gender = (int) $result['data']->gender;
                    } else {
                        $result['data']->gender = (int) 0;
                    }
                }

                if ($result['status'] == true) {
                    $result['status'] = 1;
                    if (isset($result['data']->userStatus)) {
                        $result['data']->userStatus = (int) $result['data']->userStatus;
                    }
                } else {
                    $result['status'] = 0;
                }
                $data = $result;
            } elseif ($loginType == 2) { // login with SNAPCHAT
                
            } elseif ($loginType == 3) { // login with WITH_APP
                $data = array('email' => $email, 'password' => $password);
                $result = $this->User_model->login_with_app($this->trim_data($data));
                if ($result['status'] == true) {
                    $result['status'] = 1;
                    $result['data']->userId = (int) $result['data']->userId;
                    $result['data']->loginType = (int) $result['data']->loginType;
                    $result['data']->userStatus = (int) $result['data']->userStatus;
                    if (!empty($result['data']->gender)) {
                        $result['data']->gender = (int) $result['data']->gender;
                    } else {
                        $result['data']->gender = (int) 0;
                    }
                    $result['data'] = $this->imgAbsPath($result['data']);
                } else {
                    $result['status'] = 0;
                }
                $data = $result;
            } else {
                $data = array("status" => 0, "errorMessage" => ['loginType is required'], "message" => "", 'data' => []);
            }
        } else {
            $data = array("status" => 0, "errorMessage" => $error_list, "message" => "", 'data' => []);
        }
        $data['platform'] = (int) $headers['platform'];

        $data['data'] = (object) $data['data'];
        echo json_encode($data);
    }

    public function createUsername() {
        $postData = $this->convertRawToArrayPost();
        $firstName = $postData['firstName'];
        $lastName = $postData['lastName'];
        $userName = $postData['userName'];
        $header_data = $this->getHeadersData();
        $error_exist = 'no';
        $error_list = [];

        if (empty($firstName)) {
            $error_exist = 'yes';
            $error_list[] = 'firstName is required';
        }
        if (empty($userName)) {
            $error_exist = 'yes';
            $error_list[] = 'userName is required';
        }
        if (empty($lastName)) {
            $error_exist = 'yes';
            $error_list[] = 'lastName is required';
        }

        if (empty($header_data['userId'])) {
            $error_exist = 'yes';
            $error_list[] = 'userId is required';
        }
        if (empty($header_data['accessToken'])) {
            $error_exist = 'yes';
            $error_list[] = 'accessToken is required';
        }

        if ($error_exist == 'no') {
            $data = array('userName' => $userName, 'firstName' => $firstName, "lastName" => $lastName, 'userId' => $header_data['userId'], 'accessToken' => $header_data['accessToken']);
            $result = $this->User_model->createUsername($this->trim_data($data));
            if (empty($result['errorMessage'])) {
                $result['data']->password = '';
                $result['data']->userStatus = (int) $result['data']->userStatus;
                $result['data']->userId = (int) $result['data']->userId;
                $result['data']->loginType = (int) $result['data']->loginType;

                unset($result['data']->password);
                unset($result['data']->userStatus);
                unset($result['data']->loginType);
                unset($result['data']->fcmToken);
                unset($result['data']->email);
                unset($result['data']->profilePic);
                unset($result['data']->firstName);
                unset($result['data']->lastName);
                unset($result['data']->coverPic);
            }
        } else {
            $result = array("status" => 0, "errorMessage" => $error_list, "message" => "", 'data' => (object) []);
        }
        $result['platform'] = (int) $header_data['platform'];
        echo json_encode($result);
    }

    public function updateUserStatus() {
        $postData = $this->convertRawToArrayPost();
        $userStatus = $postData['userStatus'];
        $header_data = $this->getHeadersData();
        $error_exist = 'no';
        $error_list = [];
        if ($userStatus == '') {
            $userStatus = 0;
        }
        if (empty($header_data['userId'])) {
            $error_exist = 'yes';
            $error_list[] = 'userId is required';
        }
        if (empty($header_data['accessToken'])) {
            $error_exist = 'yes';
            $error_list[] = 'accessToken is required';
        }

        if ($error_exist == 'no') {
            $data = array("userStatus" => $userStatus, 'userId' => $header_data['userId'], 'accessToken' => $header_data['accessToken']);
            $result = $this->User_model->updateUserStatus($this->trim_data($data));
        } else {
            $result = array("status" => 0, "errorMessage" => $error_list, "message" => "", 'data' => (object) []);
        }
        $result['platform'] = (int) $header_data['platform'];
        if (!empty($result['data'])) {
            if (isset($result['data']['userStatus'])) {
                $result['data']['userStatus'] = (int) $result['data']['userStatus'];
            }
        } else {
            $result['data'] = (object) [];
        }
        echo json_encode($result);
    }

    public function logout() {
        $error_exist = 'no';
        $error_list = [];
        $accessToken = $userId = "";
        $headers = $this->getHeadersData();
        if (isset($headers['userId']) && !empty($headers['userId'])) {
            $userId = $headers['userId'];
        } else {
            $error_exist = 'yes';
            $error_list[] = 'userid is required';
        }

        if (isset($headers['accessToken']) && !empty($headers['accessToken'])) {
            $accessToken = $headers['accessToken'];
        } else {
            $error_exist = 'yes';
            $error_list[] = 'accesstoken is required';
        }

        if ($error_exist == 'no') {
            $data = array('userId' => $userId, 'accessToken' => $accessToken);
            $data = $this->User_model->logout($this->trim_data($data));
        } else {
            $data = array("status" => 0, "errorMessage" => (object) $error_list, "message" => "", 'data' => []);
        }
        $data['data'] = (object) $data['data'];
        $data['platform'] = (int) $headers['platform'];
        echo json_encode($data);
    }

    public function updateImage() {
        print_r($_FILES);
        die;
        //$postData = $this->convertRawToArrayPost();
        if (!isset($_POST['imageType']) || $_POST['imageType'] == '') { // (int) (0 for Profile image, 1 for Cover image)
            $error_exist = 'yes';
            $imageType = -1;
        } else {
            $imageType = $_POST['imageType'];
        }
        $filesToUpload = $_FILES;
        $header_data = $this->getHeadersData();
        $error_exist = 'no';
        $error_list = [];
        if (empty($header_data['userId'])) {
            $error_exist = 'yes';
            $error_list[] = 'userId is required';
        }
        if (empty($header_data['accessToken'])) {
            $error_exist = 'yes';
            $error_list[] = 'accessToken is required';
        }
        if ($imageType == '') {
            $error_exist = 'yes';
            $error_list[] = 'imageType is required';
        }
        if (empty($filesToUpload)) {
            $error_exist = 'yes';
            $error_list[] = 'image is required';
        }

        /*         * *************** to check user already exists or not****************** */

        // 2.	imageType (required) (int) (0 for Profile image, 1 for Cover image)

        if ($imageType == 0) {
            $config['upload_path'] = './assets/uploads/profile_pic/';
        } elseif ($imageType == 1) {
            $config['upload_path'] = './assets/uploads/cover_pic/';
        } else {
            $error_exist = 'yes';
            $error_list[] = 'imageType is invalid';
        }
        if ($error_exist == 'no') {
            $new_name = time() . '_' . $_FILES["img_pic"]['name'];
            $config['file_name'] = $new_name;
            $config['allowed_types'] = 'gif|jpg|jpeg|png|GIF|JPG|PNG|JPEG';
            $config['max_size'] = 100000;
            $config['max_width'] = 40000;
            $config['max_height'] = 40000;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('img_pic')) {
                $error_exist = 'yes';
                $error = array('error' => $this->upload->display_errors());
                $error_list[] = strip_tags($error['error']);
            } else {
                $avatar_data = array('upload_data' => $this->upload->data());
                $img_name = $avatar_data['upload_data']['file_name'];
            }
        }
        /*         * *************** to check user already exists or not****************** */

        if ($error_exist == 'no') {
            $data = array('img_name' => $img_name, "imageType" => $imageType, 'userId' => $header_data['userId'], 'accessToken' => $header_data['accessToken']);
            $result = $this->User_model->updateImage($this->trim_data($data));
            if ($result['status'] != 0) {
                unset($result['data']->password);
                unset($result['data']->userStatus);
                unset($result['data']->loginType);
                unset($result['data']->fcmToken);
                unset($result['data']->email);
                unset($result['data']->userName);
                unset($result['data']->firstName);
                unset($result['data']->lastName);
                $result['data']->userId = (int) $result['data']->userId;
            }
        } else {
            $result = array("status" => 0, "errorMessage" => $error_list, "message" => "", 'data' => []);
        }
        $result['platform'] = (int) $header_data['platform'];
        if (!empty($result['data'])) {
            $result['data'] = $this->imgAbsPath($result['data']);
        }
        echo json_encode($result);
    }

    public function getNotificationSetting() {
        $header_data = $this->getHeadersData();
        $error_exist = 'no';
        $error_list = [];
        if (empty($header_data['userId'])) {
            $error_exist = 'yes';
            $error_list[] = 'userId is required';
        }
        if (empty($header_data['accessToken'])) {
            $error_exist = 'yes';
            $error_list[] = 'accessToken is required';
        }

        if ($error_exist == 'no') {
            $data = array('userId' => $header_data['userId'], 'accessToken' => $header_data['accessToken']);
            $result = $this->User_model->getNotificationSetting($this->trim_data($data));
        } else {
            $result = array("status" => 0, "errorMessage" => $error_list, "message" => "", 'data' => (object) []);
        }
        $result['platform'] = (int) $header_data['platform'];
        echo json_encode($result);
    }

    public function updateNotificationSetting() {
        $postData = $this->convertRawToArrayPost();

        if (empty($postData['shmoke'])) {
            $shmoke = '';
        } else {
            $shmoke = $postData['shmoke'];
        }
        if (empty($postData['match'])) {
            $match = '';
        } else {
            $match = $postData['match'];
        }
        if (empty($postData['drop'])) {
            $drop = '';
        } else {
            $drop = $postData['drop'];
        }
        if (empty($postData['smo'])) {
            $smo = '';
        } else {
            $smo = $postData['smo'];
        }
        if (empty($postData['deals'])) {
            $deals = '';
        } else {
            $deals = $postData['deals'];
        }
        if (empty($postData['smo_iou'])) {
            $smo_iou = '';
        } else {
            $smo_iou = $postData['smo_iou'];
        }

        $header_data = $this->getHeadersData();

        $error_exist = 'no';
        $error_list = [];
        if (empty($header_data['userId'])) {
            $error_exist = 'yes';
            $error_list[] = 'userId is required';
        }
        if (empty($header_data['accessToken'])) {
            $error_exist = 'yes';
            $error_list[] = 'accessToken is required';
        }

        if ($error_exist == 'no') {
            $data = array('uid' => sha1($header_data['userId'] . date('Y-m-d H:i:s') . rand()), 'seshShmoke' => $shmoke, 'seshMatch' => $match, 'seshDrop' => $drop, 'seshSmo' => $smo, 'seshDeals' => $deals, 'seshSmoIou' => $smo_iou, 'userId' => $header_data['userId'], 'accessToken' => $header_data['accessToken']);
            $result = $this->User_model->updateNotificationSetting($this->trim_data($data));
        } else {
            $result = array("status" => 0, "errorMessage" => $error_list, "message" => "", 'data' => (object) []);
        }
        $result['platform'] = (int) $header_data['platform'];
        echo json_encode($result);
    }

}
