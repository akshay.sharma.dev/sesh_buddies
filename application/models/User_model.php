<?php

Class User_model extends CI_Model {

    const table_name = "users";
    const fields = ["userId", "firstName", "lastName", "userName", "email", "password", "profilePic", "coverPic", "loginType", "userStatus", "favoriteStrain", "aboutMe", "dob", "gender", "created_at", "updated_at","quantity", "quality", "rolls", "age_verified","address", "is_deleted", "deleted_at"];
    const displayFields = ["userId", "firstName", "lastName", "userName", "email", "profilePic", "coverPic", "loginType", "userStatus", "favoriteStrain", "aboutMe", "dob", "gender", "created_at", "updated_at","quantity", "quality", "rolls", "age_verified","address","is_deleted"];

    public function getAllowedFields($data) {
        $res = array();
        foreach ($data as $key => $value) {
            if (in_array($key, self::fields)) {
                $res[$key] = $value;
            }
        }
        return $res;
    }

    public function findUserByEmail( $emailIds ){
        try{
            
            $this->db->where_in( "email",$emailIds );
            $query = $this->db->get(self::table_name);
            return $query->result_array();
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function check_user_password( $userId, $loginType , $password ){
        try{
            $this->db->where( [ "userId" => $userId, "loginType" => $loginType ] );
            $this->db->limit(1);
            $query = $this->db->get(self::table_name);
            if ($query->num_rows() == 1) {
                if ($this->encryption->decrypt($query->row('password')) == $password) {
                    return true;
                }
            }
            return false;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function check_username_exists($userName, $except_user_id = null) {
        if(empty($userName)){
            return 0;
        }
        $conditionStr = "userName ='$userName'";
        if ($except_user_id) {
            $conditionStr .= " and userId != '$except_user_id'";
        }
//        print_r($conditionStr);die;
        $this->db->where($conditionStr);
        $this->db->limit(1);
        $query = $this->db->get(self::table_name);
        return $query->num_rows();
    }

    public function check_userId_exists($userId, $data = false) {
        $this->db->where('userId', $userId);
        $this->db->limit(1);
        $query = $this->db->get(self::table_name);
        if( $data ){
            if ($query->num_rows() == 1) {
                return $query->row();
            }
            return [];
        }
        return $query->num_rows();
    }
    
    public function check_userIds_exists($userIds, $data=false) {
        $this->db->where_in('userId', $userIds);
        $query = $this->db->get(self::table_name);
        if( $data ){
            return $query->result_array();
        }
        return $query->num_rows();
    }

    public function check_email_exists($email, $data=false) {
        $this->db->select('*');
        $this->db->from(self::table_name);
        $this->db->where('email', $email);
        $query = $this->db->get();
        if( $data ){
            return $query->result_array();
        }
        return $query->num_rows();
    }

    public function forget_password($email) {
        $new_password = random_string('alnum', 6);

        $this->db->set('password', md5($new_password)); //value that used to update column  
        $this->db->where('email', $email); //which row want to upgrade  
        $this->db->update(self::table_name);

        if ($this->db->affected_rows() > 0) {
            $data = array(array("status" => true, "errorMessage" => [], "message" => "The message has been sent to your email address with New Password", "email" => $email),
                array("new_password" => $new_password)
            );
            return $data;
        } else {
            $data = array("status" => false, "errorMessage" => ["Email id doesn't exist"], "message" => "");
            return $data;
        }
    }

    public function current_user_details($user_id, $fields = "") {
        $this->load->model('Friend_model');
        if (empty($fields)) {
            $this->db->select(implode(",", self::displayFields));
        } else {
            $this->db->select($fields);
        }
        $this->db->where('userId', $user_id);
        $this->db->limit(1);
        $query = $this->db->get(self::table_name);

        if ($query->num_rows() == 1) {
            $data = (array) $query->row();
            $data['friends_count'] = $this->Friend_model->get_friends_count( $data['userId'] );
            return $data;
        }
        return false;
    }
    
    public function current_user($user_id, $fields = "") {
        if (empty($fields)) {
            $this->db->select(implode(",", self::displayFields));
        } else {
            $this->db->select($fields);
        }
        $this->db->where('userId', $user_id);
        $this->db->limit(1);
        $query = $this->db->get(self::table_name);
        if ($query->num_rows() == 1) {
            return (array)$query->row();
        }
        return false;
    }

    private function createAndupdateUserDevice($conditions, $params) {
        try{
            $this->load->model('User_device_model');
            $result = $this->User_device_model->createAndupdateUserDevice(
                    $conditions, $params
            );
            return true;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function login_with_app($params) {
        $this->load->model('Friend_model');
        $email = $params['email'];
        $password = $params['password'];
        $this->db->select(implode(",", self::fields));
        $condition = "email ='$email' and loginType=3";
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get(self::table_name);
        if ($query->num_rows() == 1) {
            if ($this->encryption->decrypt($query->row('password')) == $password) {
                $data = (array) $query->row();
                
                $this->createAndupdateUserDevice(
                    [ 'userId' => $data['userId'], 'deviceId' => $params['deviceId'] ] , 
                    ["fcmToken" => $params['fcmToken'], "platform" => $params['platform']]
                );
                
                $token_data = [
                    "user_id" => $data['userId'],
                    "email" => $data['email'],
                    "firstName" => $data['firstName']
                ];
                $this->load->library('Authorization_Token');
                $token = $this->authorization_token->generateToken($token_data);
                $data['accessToken'] = $token;
                $data['friends_count'] = $this->Friend_model->get_friends_count( $data['userId'] );
                return $data;
            }
        }
        return false;
    }
    
    public function login_with_facebook($params) {
        try {
            $this->load->model('Friend_model');
            $this->db->select(implode(",", self::displayFields));
            $this->db->where(['email' => $params['email'], 'loginType' => 1]);
            $query = $this->db->get(self::table_name);

            if ($query->num_rows()) {
                $this->User_model->updateProfile($query->row('userId') , array( 'is_deleted' => 0 ) );
                if (empty($query->row('profilePic')) && !empty($params['profilePic'])) {
                    $this->db->where("userId", $query->row('userId'));
                    $this->db->update(self::table_name, ['profilePic' => $params['profilePic']]);
                }

                $data = (array) $query->row();
                $data['is_deleted'] = 0;
                $this->createAndupdateUserDevice(
                    [ 'userId' => $data['userId'], 'deviceId' => $params['deviceId'] ] , 
                    ["fcmToken" => $params['fcmToken'], "platform" => $params['platform']]
                );
                
                
                $data['friends_count'] = $this->Friend_model->get_friends_count( $data['userId'] );
            } else {
                if ($this->check_email_exists($params['email'])) {
                    throw new Exception("Email Id is already exists.");
                } else {
                    $data = $this->registration($params);
                }
            }

            /*
             * Generate Token
             */
            $token_data = [
                "user_id" => $data['userId'],
                "email" => $data['email'],
                "firstName" => $data['firstName']
            ];
            $this->load->library('Authorization_Token');
            $token = $this->authorization_token->generateToken($token_data);
            $data['accessToken'] = $token;
            return $data;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function registration($data, $wantData = true) {
        try {
            $this->load->model('Notification_model');
            $user_data = [];
            $this->db->insert(self::table_name, $this->getAllowedFields($data));
            if ($this->db->affected_rows() > 0) {
                $user_id = $this->db->insert_id();
                $data['userId'] = $user_id;
                $this->Notification_model->enableAllNotifications( $user_id );
                if( isset( $data['deviceId'] )){
                    $this->load->model('User_device_model');
                    $this->User_device_model->addNewDevice( $data );
                }
                
                if ($wantData) {
                    $user_data = $this->current_user_details($user_id);
                    if (!$user_data) {
                        throw new Exception("Something went wrong.");
                    }
                    return $user_data;
                }
                return true;
            } else {
                throw new Exception("Something went wrong.");
            }
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function updateProfile($userId, $data_to_update, $wantData = true ) {
        try {
            if (isset($data_to_update['userName']) && $this->check_username_exists($data_to_update['userName'], $userId)) {
                throw new Exception("Username Already Exists please try another one.");
            }
            $this->db->where("userId", $userId);
            $this->db->update(self::table_name, $this->getAllowedFields($data_to_update));
            if( $wantData){
                return $this->current_user_details($userId);
            } else {
                return true;
            }
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function updateImage($data) {
        try {
            $result_be = $this->current_user_details($data['userId']);
            if ($data['imageType'] == 0) {
                $data_update = array('profilePic' => base_url() . 'assets/uploads/profile_pic/' . $data['img_name']);
            } elseif ($data['imageType'] == 1) {
                $data_update = array('coverPic' => base_url() . 'assets/uploads/cover_pic/' . $data['img_name']);
            }

            $this->db->where("userId ='" . $data['userId'] . "'");
            $this->db->update(self::table_name, $data_update);

            if (!empty(trim($result_be['profilePic'])) && $data['imageType'] == 0) {
                $img_url = $result_be['profilePic'];
                $img_parts = explode("/", $img_url);
                $profile_pic_name = end($img_parts);
                $path = './assets/uploads/profile_pic/' . $profile_pic_name;
                unlink($path);
            }
            if (!empty(trim($result_be['coverPic'])) && $data['imageType'] == 1) {
                $img_url = $result_be['coverPic'];
                $img_parts = explode("/", $img_url);
                $cover_pic_name = end($img_parts);
                $path = './assets/uploads/cover_pic/' . $cover_pic_name;
                unlink($path);
            }

            return array_merge($result_be, $data_update);
        } catch (Exception $ex) {
            throw new $ex;
        }
    }

}

?>