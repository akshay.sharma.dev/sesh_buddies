<?php

Class Sesh_model extends CI_Model {

    const table_name = "seshes";
    const fields = ["userId", "type", "point", "strain", "gram", "date", "time", "location", "utensils" , "created_at", "updated_at", "flavour", "status"];
    
    public function __construct() {
        parent::__construct();
        $this->load->helper('common-utility');
    }
    
    public function getAllowedFields($data) {
        $res = array();
        foreach ($data as $key => $value) {
            if (in_array($key, self::fields)) {
                $res[$key] = $value;
            }
        }
        return $res;
    }
    
    public function save_sesh( $data ){
        try {
            $this->db->insert( self::table_name , $this->getAllowedFields($data));
            return true;
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function save_sesh_and_assign_buddies( $data ){
        try {
            $seshId ;
            $this->db->trans_begin();
            $this->db->insert( self::table_name , $this->getAllowedFields($data));
            
            if ($this->db->affected_rows() > 0) {
                $seshId = $this->db->insert_id();
                
                //Added Owner as member
                $this->db->insert( 'sesh_and_boddies' , [ "sesh_id" => $seshId, "user_id" => $data['userId'] , "status" => Sesh_boddy_status::ACCEPTED ]  );
//                print_r( $data['members']);
                foreach( $data['members'] as $user ){
                    if( $user && $user['userId'] && $data['userId'] !=$user['userId'] ) {
                        $this->db->insert( 'sesh_and_boddies' , [ "sesh_id" => $seshId, "user_id" => $user['userId'] , "status" => Sesh_boddy_status::PENDING ]  );
                    }
                }
            } else {
                throw new Exception("Something went wrong.");
            }
            
            $this->db->trans_complete();
            if ($this->db->trans_status() === false) {
                throw new Exception("Something went wrong.");
            }
            $this->db->trans_commit();
            return $seshId;
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            throw $ex;
        }
    }
    
    public function endSesh( $sesh_id, $status ){
        try{
            $this->db->where(["id" => $sesh_id ]);
            $this->db->update(self::table_name, ['status' => $status ] );
            return true;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function getSeshByUser( $user_id, $getSeshesIds=[], $params=[] ){
        try{
            $query  = "select * from ". self::table_name. " where ( userId=".$user_id;
            if( count( $getSeshesIds ) ) {
                $query .= " or ID in ( ". implode($getSeshesIds, ",")." ) ) ";
            } else {
                $query  .= ") ";
            }
            if( isset($params['type']) ){
                $query.=" and type='".$params['type']."' ";
            }
            
            if( isset($params['status']) ){
                $query.=" and status=".$params['status'];
            }
            
            if( isset($params['created_at']) ){
                $query.=" and created_at >= '".$params['created_at']."'";
            }
            
//            echo "$query";
            $query = $this->db->query( $query );
            return $query->result_array();
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    
    
    public function all( $type, $userData ){
        try{
            $findParamStr = "";
            if( $type ){
                $findParamStr .= 'type ='.$type." and ";
            }
            if($userData && $userData->deleted_at){
                $findParamStr .= "created_at >= '".$userData->deleted_at."'";
            }
            
            echo $findParamStr;
            $this->db->where($findParamStr);
            $query = $this->db->get(self::table_name);
            return $query->result_array();
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function get_detail( $params ){
        try{
            $this->db->where($params);
            $this->db->limit(1);
            $query = $this->db->get(self::table_name);

            return $query->row();
        } catch (Exception $ex) {
            throw $ex;
        }
    }
}

?>