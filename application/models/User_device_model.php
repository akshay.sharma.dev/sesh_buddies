<?php

Class User_device_model extends CI_Model {

    const table_name = "user_devices";
    const fields = ["userId", "platform","deviceId", "fcmToken", "created_at", "updated_at"];
    const SERVER_API_KEY = "AAAAt6YUB0k:APA91bEHde0ZC-AClwvuEfl3TfUxuuYJAP8KY98lN9FBeDMd6g4VrtuFsGX2RsDds_l5QZSocgc2Q6-jWe4Q-xkD-87yiIMHGBSk__vjx6Ld9tWOSHHVIZQyXvN00rAQPWKKgDr5KNZm";

    public function __construct() {
        parent::__construct();
        $this->load->helper('common-utility');
        $this->load->library('Client');
    }
    
    public function getAllowedFields($data) {
        $res = array();
        foreach ($data as $key => $value) {
            if (in_array($key, self::fields)) {
                $res[$key] = $value;
            }
        }
        return $res;
    }
    
    public function addNewDevice( $params ){
        try{
            $data = [
                "userId" => $params['userId'],
                "deviceId" => $params['deviceId'],
                "platform" => $params['platform'],
                "fcmToken" => $params['fcmToken'],
            ];
            $this->db->insert(self::table_name, $data );
            if ($this->db->affected_rows() > 0) {
                $user_device_id = $this->db->insert_id();
                return $user_device_id;
            } else {
                throw new Exception("Something went wrong.");
            }
            
            return false;
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    public function deleteDevice( $params ){
        try{
            $condition = [
                "userId" => $params['userId']
            ];
            
            if( isset( $params['deviceId'] ) ){
                $condition["deviceId"] = $params['deviceId'];
            }
            
            $this->db->where($condition);
            $this->db->delete( self::table_name ); 
            
            return true;
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    private function updatePlatformAndToken( $condition, $params ){
        try{
            $data = [
                "platform" => $params['platform'],
                "fcmToken" => $params['fcmToken']
            ];
            
            $this->db->where(["userId" => $condition['userId'], 'deviceId' => $condition['deviceId'] ]);
            $this->db->update(self::table_name, $data );
            
            return true;
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function createAndupdateUserDevice( $condition , $params){
        try{
            $this->db->where(["userId" => $condition['userId'], 'deviceId' => $condition['deviceId'] ]);
            $this->db->limit(1);
            $query = $this->db->get(self::table_name);
            if( $query->num_rows() > 0 ){
                //update
                $this->updatePlatformAndToken( $condition , $params );
            } else {
                //create
                $params['deviceId'] = $condition['deviceId'];
                $params['userId'] = $condition['userId'];
                $this->addNewDevice( $params );
            }
            
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function getDeviceTokens( $userIds ){
        try{
            $this->db->select('ud.id, ud.userId, ud.platform, ud.deviceId, ud.fcmToken, n.shmoke, n.match, n.drop, n.smo, n.deals, n.smo_iou');
            
            $this->db->from(self::table_name . ' as ud ');
            $this->db->where_in("ud.userId", $userIds);
            $this->db->join('notification_settings as n', 'ud.userId = n.userId');
            $query = $this->db->get();
            return $query->result_array();
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function sendNotifications($userIds, $title, $notificationData, $type = null){
        try{
            $this->load->model('Push_notification_model');
//            print_r( $userIds );
            $user_devices = $this->getDeviceTokens($userIds);
//            print_r( $user_devices );
            $notificationData['type'] = $type;
            $type = strtolower( $type ) ;
            $userAndroidTokens = [];
            $userIOSTokens = [];
            $userTokens = [];
            
            foreach ( $user_devices as $user ){
                if( $type ){
                    if( isset($user[$type]) &&  $user[$type] == 1 ){
                        $userTokens[] =  $user['fcmToken'];
                    }
                }else {
                    $userTokens[] =  $user['fcmToken'];
                }
                
            }
            
            $pushNotifications = [];
            foreach ( $userIds as $userId ) {
                $pushNotifications[] = [
                    'userId' => $userId,
                    'title' => $title,
                    'payload' => json_encode($notificationData)
                ];
            }
 
            if(count( $pushNotifications )){
                $this->Push_notification_model->save_many($pushNotifications);
            }
            
            if( count( $userTokens ) ){
                
                $headers = [
                    "Authorization: key=". self::SERVER_API_KEY
                ]; 

                $payload = [
                    'registration_ids' => $userTokens,
                    "priority" => "normal",
//                    "notification" => [
//                        "body" => $title,
//                        "title" => "Sesh Buddies",
//                        "icon" => "new",
//                        "click_action" => $notificationData['action']
//                      ],
                    'data' => [
                        "sesh_id" => $notificationData['sesh_id'],
                        "click_action" => $notificationData['action'],
                        "body" => $title,
                        "title" => "Sesh Buddies",
                    ]
                ];

                $r = $this->client->request(
                        'POST', 
                        'https://fcm.googleapis.com/fcm/send', 
                        ['json' => $payload], 
                        ['headers' => $headers]
                );
            }
            
            
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    
}

?>