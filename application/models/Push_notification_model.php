<?php

Class Push_notification_model extends CI_Model {

    const table_name = "push_notifications";
    const fields = ["userId","title", "is_read", "payload", "created_at", "updated_at"];

    public function getAllowedFields($data) {
        $res = array();
        foreach ($data as $key => $value) {
            if (in_array($key, self::fields)) {
                $res[$key] = $value;
            }
        }
        return $res;
    }
    
    public function save_many( $data ){
        try {
            $this->db->insert_batch( self::table_name , $data);
            return $this->db->insert_id();;
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function save ( $data ){
        try {
            $this->db->insert( self::table_name , $this->getAllowedFields($data));
            return true;
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function get( $params ){
        try{
            $this->db->where( $params );
            $this->db->from(self::table_name);
            $this->db->order_by("id", "desc");
            $query = $this->db->get();
            return $query->result_array();
        } catch (Exception $ex) {
            throw $ex;
        }
    }
}

?>