
<?php

class Friend_model extends CI_Model
{
    const table_name = "friends";
    const fields = ["id", "user_id", "friend_id", "created_at", "updated_at"];

    private function getAllowedFields($data)
    {
        $res  = array();
        foreach ($data as $key => $value) {
            if (in_array($key, self::fields)) {
                $res[$key] = $value;
            }
        }
        return $res;
    }


    public function delete_friends_belongsTo($user_id)
    {
        try {
            $this->db->where("(user_id='" . $user_id . "' OR friend_id='" . $user_id . "')", NULL, FALSE);
            $this->db->delete(self::table_name);
            return true;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function add_friends($user_id, $friend_ids)
    {
        try {
            $this->load->model('User_model');
            // print_r($friend_ids);

            $number_of_existing_user = $this->User_model->check_userIds_exists($friend_ids);
            // var_dump($number_of_existing_user);

            if (count($friend_ids) !== $number_of_existing_user) {
                throw new Exception("Please provide valid user IDs");
            }

            $data_to_insert = [];
            $this->db->where('user_id', $user_id);
            $query = $this->db->get(self::table_name);

            if ($query->num_rows()) {
                $friends = (array) $query->result_array();
                $friend_exist_ids = [];
                foreach ($friends as $friend) {
                    $friend_exist_ids[] = $friend['friend_id'];
                }

                // print_r($friend_exist_ids);
                foreach ($friend_ids as $friend) {
                    if (!is_numeric($friend)) {
                        throw new Exception("Please provide valid friend ID");
                    }
                    if ($friend == $user_id) {
                        throw new Exception("User ID and friend ID must unique.");
                    }

                    if (!in_array($friend, $friend_exist_ids)) {
                        $data_to_insert[] = ["user_id" => $user_id, "friend_id" => $friend];
                    }
                }
            } else {
                foreach ($friend_ids as $friend) {
                    if (!is_numeric($friend)) {
                        throw new Exception("Please provide valid friend ID");
                    }
                    if ($friend == $user_id) {
                        throw new Exception("User ID and friend ID must unique.");
                    }

                    $data_to_insert[] = ["user_id" => $user_id, "friend_id" => $friend];
                }
            }

            // print_r( $data_to_insert );

            if (count($data_to_insert)) {

                $this->db->insert_batch(self::table_name, $data_to_insert);
            }

            return true;
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            throw $ex;
        }
    }

    public function get_friends($user_id, $select = "", $join = true)
    {
        if (!empty($select)) {
            $this->db->select($select);
        } else {
            $this->db->select('friend_id as userId, firstName, lastName, userName, email, coverPic, profilePic, userStatus, gender,quantity,quality,rolls');
        }

        $this->db->where("user_id", $user_id);
        if ($join) {
            $this->db->join('users', 'userId = friend_id');
        }
        $query = $this->db->get(self::table_name);
        return $query->result_array();
    }

    public function get_friends_by_ids($user_id, $friendIds, $select = "")
    {
        if (!empty($select)) {
            $this->db->select($select);
        } else {
            $this->db->select('userId, firstName, lastName, userName, email, coverPic, profilePic, userStatus, gender');
        }
        $this->db->where("user_id", $user_id);
        if (count($friendIds)) {
            $this->db->where_in("friend_id", $friendIds);
        }

        $query = $this->db->get(self::table_name);
        return $query->result_array();
    }

    public function get_friends_count($user_id)
    {
        $this->db->where("user_id", $user_id);
        $query = $this->db->get(self::table_name);
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    /**
     * remove the friends from the list 
     * @function : get_remove_friends_data()
     * @funtionality : just change the is_deleted peramter to 1 if user want to delete there friend
     */
    public function get_remove_friends_data($user_id, $friends_id)
    {
        // var_dump($number_of_existing_user);
        $select = '*';
        $is_friend_existing =  $this->get_friends_by_ids($user_id, $friends_id, $select);

        if (count($friends_id) !== count($is_friend_existing)) {
            return false;
        }
        return $is_friend_existing;
    }

    /**
     * @funtionality : - remove user from buddy list
     *
     */
    public function delete_friend($user_id,$friend_id)
    {
        $data = ['is_deleted'=>'1'];
        $this->db->where('user_id',$user_id);
        $this->db->where_in('id', $friend_id);
        $this->db->update(self::table_name,$data);
        if($count = $this->db->affected_rows())
        {
            return $count;
        }
        return false;
       
    }
}
?>