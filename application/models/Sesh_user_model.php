<?php

Class Sesh_user_model extends CI_Model {
    
    const table_name = "sesh_and_boddies";
    const fields = ["id", "sesh_id","user_id","status", "reviewed", "created_at"];
   
     public function __construct() {
        parent::__construct();
        $this->load->helper('common-utility');
    }
    
    public function getAllowedFields($data) {
        $res = array();
        foreach ($data as $key => $value) {
            if (in_array($key, self::fields)) {
                $res[$key] = $value;
            }
        }
        return $res;
    }
    
    public function getSeshUsers( $seshId, $user_id = null ){
        $this->db->where('sesh_id', $seshId );
        if( $user_id ){
            $this->db->where_not('user_id', $user_id );
        }
        $query = $this->db->get(self::table_name);
        return $query->result_array();
    }
   
    public function getSeshBy( $params, $joins=false ){
        try{
            $whereParams = $this->getAllowedFields($params);
            $this->db->where($whereParams);
            if( isset($params['in_status']) && count( $params['in_status'] ) ){
                $this->db->where_in('status', $params['in_status']);
            }
            
            if ( $joins ){
                $this->db->join('users', 'userId = user_id');
            }
            
            $query = $this->db->get(self::table_name);
            return $query->result_array();
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function UpdateSeshByUserId( $sesh_id,$user_id, $status ){
        try{
            $this->db->where(["sesh_id" => $sesh_id, "user_id" => $user_id ]);
            $this->db->update(self::table_name, ['status' => $status ] );
            return true;
        } catch (Exception $ex) {
            throw $ex;
        }
    }
}

?>