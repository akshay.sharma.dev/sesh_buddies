<?php

Class Sesh_review_model extends CI_Model {

    const table_name = "sesh_reviews";
    const fields = ["reviewer_id", "sesh_id","userId", "quantity", "quality", "rolls", "review", "created_at", "updated_at"];

    public function getAllowedFields($data) {
        $res = array();
        foreach ($data as $key => $value) {
            if (in_array($key, self::fields)) {
                $res[$key] = $value;
            }
        }
        return $res;
    }
    
    public function save_review( $data ){
        try {
            $this->db->insert_batch( self::table_name , $data);
            return $this->db->insert_id();;
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function getSeshReviewByUser( $params ){
        try{
            $conditionStr = "reviewer_id=".$params['reviewer_id']." ";
            if( !empty($params['sesh_id']) ) {
                $conditionStr.= " and sesh_id= " . $params['sesh_id'];
            }
            $this->db->where( $conditionStr );
//            $this->db->where("sesh_id", $params['sesh_id']);
            $query = $this->db->get(self::table_name);
            return $query->result_array();
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function getReviews( $params ){
        try{
//            $this->db->select("sr.id,sr.reviewer_id,sr.userId,sr.sesh_id,sr.quantity,sr.quality,sr.rolls,sr.review,sr.created_at,sr.updated_at");
            
            $this->db->where( $params );
            $this->db->from(self::table_name);
//            $this->db->where("sesh_id", $params['sesh_id']);
            $query = $this->db->get();
//            $this->db->join('users as u', 'sr.reviewer_id = u.userId');
            return $query->result_array();
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function getReviewsAvg( $userId ){
        $query = $this->db->query("SELECT AVG(quantity) AS quantity, AVG(quality) AS quality, AVG(rolls) AS rolls FROM " . self::table_name . " where userId='".$userId."' ");
        return $query->row();
    }
}

?>