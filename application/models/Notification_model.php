<?php

class Notification_model extends CI_Model {
    const table_name = "notification_settings";
    const fields = ["id", "userId", "shmoke", "match", "drop", "smo", "deals", "smo_iou", "created_at", "updated_at"];
    
    private function getAllowedFields( $data ){
        $res  = array();
        foreach ( $data as $key => $value ){
            if(in_array( $key, self::fields ) ){
                $res[$key] = $value;
            }
        }
        return $res;
    }
    
    public function enableAllNotifications( $userId ){
        $this->saveSettings( [ 
            'userId' => $userId
        ]);
        return true;
    }


    public function saveSettings( $data ){
        try {
            $this->db->insert( self::table_name , $this->getAllowedFields($data));
            return true;
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function getSettingsByUser( $user_id ){
        try {
            $this->db->select("id, userId, shmoke, match, drop, smo, deals, smo_iou");
            $this->db->where("userId", $user_id);
            $query = $this->db->get(self::table_name);
            return $query->num_rows();
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function updateNotificationSetting($user_id, $data ){
        try{
            $this->db->where('userId', $user_id);
            $this->db->update( self::table_name , $this->getAllowedFields($data) ); 
            return true;
        } catch (Exception $ex) {
            $this->db->trans_rollback();
            throw $ex;
        }
    }
    
    public function collectionModel( $data ){
           $fields = ["shmoke", "match", "drop", "smo", "deals", "smo_iou"];
           foreach ( $fields as $type){
               if( isset( $data[$type] ) ){
                   $data[$type] = (int)$data[$type];
               }
           }
           return $data;
    }
    
    public function getNotificationSetting( $user_id ){
        try {
            $this->db->select("id, userId, shmoke, match, drop, smo, deals, smo_iou");
            $this->db->where("userId", $user_id);
            $query = $this->db->get(self::table_name);
            return $this->collectionModel( (array) $query->row());
        } catch (Exception $ex) {
            throw $ex;
        }
    }
}
?>