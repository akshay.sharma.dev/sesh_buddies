<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'third_party/php-jwt/JWT.php';
require_once APPPATH . 'third_party/php-jwt/BeforeValidException.php';
require_once APPPATH . 'third_party/php-jwt/ExpiredException.php';
require_once APPPATH . 'third_party/php-jwt/SignatureInvalidException.php';
require_once APPPATH . 'helpers/common-utility_helper.php';

use \Firebase\JWT\JWT;


class Authorization_Token {

    /**
     * Token Key
     */
    protected $token_key;

    /**
     * Token algorithm
     */
    protected $token_algorithm;

    /**
     * Request Header Name
     */
    protected $token_header = ['authorization', 'Authorization', "accessToken", "AccessToken", "Accesstoken"];

    /**
     * Token Expire Time
     * ----------------------
     * ( 1 Day ) : 60 * 60 * 24 = 86400
     * ( 1 Hour ) : 60 * 60     = 3600
     */
    protected $token_expire_time = 86400;

    public function __construct() {
        $this->CI = & get_instance();
        /**
         * jwt config file load
         */
        $this->CI->load->config('jwt');
        /**
         * Load Config Items Values 
         */
        $this->token_key = $this->CI->config->item('jwt_key');
        $this->token_algorithm = $this->CI->config->item('jwt_algorithm');
        $this->CI->load->model('User_model');
    }

    /**
     * Generate Token
     * @param: user data
     */
    public function generateToken($data) {
        try {
            $data['time'] = strtotime('now');
            return JWT::encode($data, $this->token_key, $this->token_algorithm);
        } catch (Exception $e) {
            return 'Message: ' . $e->getMessage();
        }
    }

    /**
     * Validate Token with Header
     * @return : user informations
     */
    public function validateToken() {
        /**
         * Request All Headers
         */
        $headers = $this->CI->input->request_headers();

        /**
         * Authorization Header Exists
         */
        try {
            $token_data = $this->tokenIsExist($headers);
            if ($token_data) {
            
                $token_decode = JWT::decode($headers[$token_data], $this->token_key, array($this->token_algorithm));
                if (empty($token_decode) || !is_object($token_decode)) {
                    throw new Exception("Forbidden");
                }
//                print_r($token_decode);
                // Check User ID (exists and numeric)
                if (empty($token_decode->user_id) OR ! is_numeric($token_decode->user_id)) {
                    throw new Exception("User ID Not Define!");
                    // Check Token Time
                } else if ( !isset ($token_decode->time) || empty($token_decode->time OR ! is_numeric($token_decode->time))) {
                    throw new Exception("Token Time Not Define!");
                } else {
                    /**
                     * Check Token Time Valid 
                     */
                    $userObj = $this->CI->User_model->check_userId_exists( $token_decode->user_id, true );
//                    print_r($userObj);
                    
                    if( !$userObj ){
                        throw new Exception("Invalid Token.");
                    } else if($userObj->is_deleted){
                        throw new Exception("Your account has closed.");
                    }
                    $time_difference = strtotime('now') - $token_decode->time;
                    if ($time_difference >= $this->token_expire_time) {
                        throw new Exception("Token Time Expire.");
                    } else {
                        /**
                         * All Validation False Return Data
                         */
                        return $token_decode;
                    }
                }
                

            } else {
                // Authorization Header Not Found!
                throw new Exception("Authorization Header Not Found");
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Validate Token with POST Request
     */
    public function validateTokenPost() {
        if (isset($_POST['token'])) {
            $token = $this->CI->input->post('token', TRUE);
            if (!empty($token) AND is_string($token) AND ! is_array($token)) {
                try {
                    /**
                     * Token Decode
                     */
                    try {
                        $token_decode = JWT::decode($token, $this->token_key, array($this->token_algorithm));
                    } catch (Exception $e) {
                        return ['status' => FALSE, 'message' => $e->getMessage()];
                    }

                    if (!empty($token_decode) AND is_object($token_decode)) {
                        // Check User ID (exists and numeric)
                        if (empty($token_decode->id) OR ! is_numeric($token_decode->id)) {
                            return ['status' => FALSE, 'message' => 'User ID Not Define!'];

                            // Check Token Time
                        } else if (empty($token_decode->time OR ! is_numeric($token_decode->time))) {

                            return ['status' => FALSE, 'message' => 'Token Time Not Define!'];
                        } else {
                            /**
                             * Check Token Time Valid 
                             */
                            $time_difference = strtotime('now') - $token_decode->time;
                            if ($time_difference >= $this->token_expire_time) {
                                return ['status' => FALSE, 'message' => 'Token Time Expire.'];
                            } else {
                                /**
                                 * All Validation False Return Data
                                 */
                                return ['status' => TRUE, 'data' => $token_decode];
                            }
                        }
                    } else {
                        return ['status' => FALSE, 'message' => 'Forbidden'];
                    }
                } catch (Exception $e) {
                    return ['status' => FALSE, 'message' => $e->getMessage()];
                }
            } else {
                return ['status' => FALSE, 'message' => 'Token is not defined.'];
            }
        } else {
            return ['status' => FALSE, 'message' => 'Token is not defined.'];
        }
    }

    /**
     * Token Header Check
     * @param: request headers
     */
    private function tokenIsExist($headers) {
        try {
            if (!empty($headers) AND is_array($headers)) {
                foreach ($this->token_header as $key) {
                    if (array_key_exists($key, $headers) AND ! empty($key))
                        return $key;
                }
            }
            throw new Exception("Token is not defined.");
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    /**
     * Fetch User Data
     * -----------------
     * @param: token
     * @return: user_data
     */
    public function userData() {
//        try{
//            /**
//            * Request All Headers
//            */
//           $headers = $this->CI->input->request_headers();
//           /**
//            * Authorization Header Exists
//            */
//           $token_data = $this->tokenIsExist($headers);
//           if ($token_data) {
//               
//                /**
//                 * Token Decode
//                 */
//                $token_decode = JWT::decode($headers[$token_data], $this->token_key, array($this->token_algorithm));
//                
//                if (!empty($token_decode) AND is_object($token_decode)) {
//                    return $token_decode;
//                } else {
//                    throw new Exception("Forbidden");
//                }
//               
//           } else {
//               throw new Exception("Authorization Header Not Found");
//           }
//        } catch (Exception $ex) {
//            throw $ex;
//        }
        return $this->validateToken();
    }
    
    public function getPlatformOrException(){
        try{
            $headerKey = 'Platform';
            $headers = $this->CI->input->request_headers();
            // print_r($headers); die;
            if( !isset( $headers[ $headerKey ] ) ){
                throw new Exception("Platform params does not exist, please provide valid platform");
            }
            
            if( !($headers[ $headerKey ] == (string)PLATFORM::IOS || $headers[ $headerKey ] == (string)PLATFORM::ANDROID) ){
                throw new Exception("please provide platform with valid value.");
            }
            return $headers[ $headerKey ];
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
    public function getPlatform(){
        try{
            $headerKey = 'Platform';
            $headers = $this->CI->input->request_headers();
            if( !isset( $headers[ $headerKey ] ) || empty($headers[ $headerKey ]) ){
                return null;
            }
            if( $headers[ $headerKey ] !== PLATFORM::IOS || $headers[ $headerKey ] !== PLATFORM::ANDROID ){
                return null;
            }
            
            return $headers[ $headerKey ];
        } catch (Exception $ex) {
            throw $ex;
        }
    }
    
}
