<?php
class Helper{
    static function getHeadersData() {
        $data = [];
        $headers = getallheaders();
        if (isset($headers['Userid']) && !empty($headers['Userid'])) {
            $data['userId'] = $headers['Userid'];
        }
        if (isset($headers['Accesstoken']) && !empty($headers['Accesstoken'])) {
            $data['accessToken'] = $headers['Accesstoken'];
        }
        if (isset($headers['Platform']) && $headers['Platform'] != "") {
            $data['platform'] = $headers['Platform'];
        }
        return $data;
    }
    
    static function rawInput() {
        $_POST = (array) json_decode(file_get_contents('php://input'), TRUE);
    }
    
    static function isAssoc(array $arr1) {
        if(array_keys($arr1) !== range(0, count($arr1) - 1)) 
            return true;
        else
            return false;
    }

    
    static function success( $msg, $data = [] ){
        $finalOut = [];
        if( !count( $data ) ){
            $finalOut = new stdClass();
        } else if( self::isAssoc( $data ) ){
            $finalOut = $data;
        } else {
            $finalOut['list'] = (array)$data;
        }
        return array("status" => 1, "errorMessage" => [], "message" => $msg , 'data' => $finalOut );
    }


    static function error( $err ){
        if(is_string($err) ){
            $err = [$err];
        }
        $finalErrors = [];
        foreach ( $err as $key => $er ){
            $finalErrors[] = $er ;
        }
        
        return array("status" => 0, "errorMessage" => $finalErrors, "message" => "", 'data' => new stdClass() );
    }
    
    static function userCollection( $data ){
        if( isset($data['loginType']) ){
            $data['loginType'] = (int) $data['loginType'];
        }
        
        if( isset($data['gender']) ){
            if( $data['gender'] === NULL ) {
                $data['gender'] = 0;
            } else {
                $data['gender'] = ( int ) $data['gender'];
            }
        }
        if( isset($data['userStatus']) ){
            $data['userStatus'] = (int) $data['userStatus'];
        }
        
        if( isset($data['age_verified']) ){
            $data['age_verified'] = (int) $data['age_verified'];
        }
        
        if( isset($data['quantity']) ){
            $data['quantity'] = (int) $data['quantity'];
        }
        if( isset($data['quality']) ){
            $data['quality'] = (int) $data['quality'];
        }
        
        if( isset($data['rolls']) ){
            $data['rolls'] = (int) $data['rolls'];
        }

        if( isset($data['is_deleted']) ){
            $data['is_deleted'] = (int) $data['is_deleted'];
        }
        
        return $data;
    }
    
    static function reviewCollection( $data ){
        
        if( isset($data['quantity']) ){
            $data['quantity'] = (int) $data['quantity'];
        }
        if( isset($data['quality']) ){
            $data['quality'] = (int) $data['quality'];
        }
        
        if( isset($data['rolls']) ){
            $data['rolls'] = (int) $data['rolls'];
        }
        
        return $data;
    }
    
    static function SeshCollection( $data ){
        if( isset($data['status']) ){
            $data['status'] = (int) $data['status'];
        }
        
        return $data;
    }
    
}


class LOGIN_PROVIDER{
    const FACEBOOK = 1;
    const SNAPCHAT = 2;
    const APP = 3;
}

class Sesh_boddy_status{
    const ACCEPTED = 1;
    const END = 0;
    const REJECTED= 3;
    const PENDING=2;
}

class Sesh_status{
    const LIVE = 1;
    const END = 0;
    const CANCELED = 2;
}

class PLATFORM{
    const IOS = 1;
    const ANDROID = 0;
}

class SESH_ACTION{
    const INVITED = 1;
    const CANCELED = 2;
    const REVIEWED = 3;
    const ACCEPTED = 4;
    const REJECTED = 5;
    const ENDED = 6; // <= New Added
}